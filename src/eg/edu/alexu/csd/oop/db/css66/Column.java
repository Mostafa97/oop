package eg.edu.alexu.csd.oop.db.css66;

import java.util.ArrayList;

public class Column {
	private ArrayList<Object> Column;
	private String type;
	private String name;
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void setColumn(ArrayList<Object> col)
	{
		this.Column = col;
	}
	
	public ArrayList<Object> getColumn()
	{
		return Column;
	}
}
