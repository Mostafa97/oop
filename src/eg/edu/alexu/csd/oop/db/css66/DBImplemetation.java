package eg.edu.alexu.csd.oop.db.css66;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eg.edu.alexu.csd.oop.db.Database;

public class DBImplemetation implements Database {
	// String current = System.getProperty("user.dir") + Separator +
	// "Databases" + Separator;
	ArrayList<database> databases = new ArrayList();
	database currentdb = new database();
	Table currentt = new Table();
	String currentDatabase = null;
	String Separator = System.getProperty("file.separator");

	@Override
	public String createDatabase(String databaseName, boolean dropIfExists) {
		File database = new File(databaseName);
		String dbname = database.getName();
		dbname = dbname.toLowerCase();
		dbname = dbname.trim();
		boolean exists = false;
		if (new File(databaseName).exists()) {
			database d = new database();
			for (int i = 0; i < databases.size(); i++) {
				if (databases.get(i).getName().equals(dbname)) {
					currentdb.setDatabase(databases.get(i).getDatabase());
				}
			}
			exists = true;
		} else {
			database.mkdirs();
			if (dropIfExists) {
				if (database.exists()) {
					try {
						executeStructureQuery("DROP Database " + dbname + ";");
						executeStructureQuery("Create Database " + dbname + ";");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					try {
						executeStructureQuery("Create Database " + dbname + ";");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			} else {
				if (!database.exists()) {
					try {
						executeStructureQuery("Create Database " + dbname);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return database.getAbsolutePath();
	}

	@Override
	public boolean executeStructureQuery(String query) throws SQLException {
		query = query.trim();
		query = query.toLowerCase();
		String[] parts = query.split("\\W+");
		if (parts[0].equalsIgnoreCase("create")) {
			if (parts[1].equalsIgnoreCase("table")) {
				String name = parts[2];
				if (currentdb.getName() == null) {
					throw null;
				}
				File table = new File(System.getProperty("user.dir") + Separator + "Databases" + Separator
						+ currentdb.getName() + Separator + name + ".xml");
				Table t = new Table();
				t = makeTable(query);
				t.setName(name);
				t.setTable(makeTable(query).getTable());
				saveXMLFile(t);
				saveDTDFile(t);
				if (containsTable(t, currentdb) != null) {
					return false;
				} else {
					currentt.setName(name);
					currentt.setTable(t.getTable());
					currentdb.addTable(t);
					return true;
				}
				/*
				 * if (!table.exists()) { Table t = new Table(); t =
				 * makeTable(query); t.setName(name);
				 * t.setTable(makeTable(query).getTable()); saveXMLFile(t);
				 * saveDTDFile(t); return true; } else { return false; }
				 */
			} else if (parts[1].equalsIgnoreCase("database")) {
				File newDb = new File(parts[2]);
				database d = new database();
				d.setName(parts[2]);

				if (containsdb(d) != null) {
					currentdb = containsdb(d);

				} else {
					databases.add(d);
					currentdb = d;
					newDb.mkdirs();
				}

				if (!newDb.exists()) {
					newDb.mkdirs();
					currentDatabase = parts[2];

					return true;
				} else {
					currentDatabase = parts[2];
					return false;
				}

			}

		} else if (parts[0].equalsIgnoreCase("drop")) {
			if (parts[1].equalsIgnoreCase("table")) {
				File table = new File("Databases" + Separator + currentDatabase + Separator + parts[2] + ".xml");
				File dtd = new File("Databases" + Separator + currentDatabase + Separator + parts[2] + ".dtd");
				String name = parts[2].trim().toLowerCase();
				Table t = new Table();
				t.setName(name);
				t.setTable(makeTable(query).getTable());
				if (containsTable(t, currentdb) != null) {
					deleteTable(t.getName(), currentdb);
					table.delete();
					dtd.delete();
					return true;
				} /*
					 * if (table.exists()) { table.delete(); dtd.delete(); }
					 * else { return false; }
					 */
			} else if (parts[1].equalsIgnoreCase("database")) {
				File newDb = new File("Databases" + Separator + parts[2]);
				database d = new database();
				d.setName(parts[2].trim().toLowerCase());
				if (containsdb(d) != null) {
					deletedb(parts[2].trim().toLowerCase());
					return true;
				} else {

				}
				if (newDb.exists()) {
					String[] list = newDb.list();
					for (String s : list) {
						File current1 = new File(newDb.getPath(), s);
						current1.delete();
					}
					newDb.delete();
					currentDatabase = null;
					return true;
				} else {
					return false;
				}
			}
		}

		return false;
	}

	public Table containsTable(Table t, database d) {
		ArrayList<Table> temp = d.getDatabase();
		for (int i = 0; i < temp.size(); i++) {
			String hand = temp.get(i).getName();
			if (hand.equals(t.getName())) {
				return temp.get(i);
			}
		}
		return null;
	}

	public database containsdb(database d) {
		for (int i = 0; i < databases.size(); i++) {
			String hand = databases.get(i).getName();
			if (hand.equals(d.getName())) {
				return databases.get(i);
			}
		}
		return null;
	}

	public void deleteTable(String name, database d) {
		ArrayList<Table> temp = d.getDatabase();
		for (int i = 0; i < temp.size(); i++) {
			String hand = temp.get(i).getName();
			if (hand.equals(name)) {
				d.getDatabase().remove(i);
			}
		}
	}

	public void deletedb(String name) {
		for (int i = 0; i < databases.size(); i++) {
			String hand = databases.get(i).getName();
			if (hand.equals(name)) {
				databases.remove(i);
			}
		}
	}

	@Override
	public Object[][] executeQuery(String query) throws SQLException {
		// SELECT * FROM table_Name
		// SELECT col1_name, col2_name, col3_name FROM table_Name
		Table loadedTable = new Table();
		Table toReturn = new Table();
		query = query.trim();
		query = query.toLowerCase();
		String[] partsOfQuery = query.split("\\W+");
		String givenName = partsOfQuery[2];
		if (givenName.equalsIgnoreCase("from")) {
			givenName = partsOfQuery[3];
		}
		givenName = givenName.trim();
		File myDatabase = new File(Separator + "Databases" + Separator + currentDatabase);
		ArrayList<Table> tables = currentdb.getDatabase();
		if (currentdb == null) {
			throw new NullPointerException(query);
		}
		if (query.contains("*")) {
			for (int i = 0; i < tables.size(); i++) {
				if (tables.get(i).getName().equalsIgnoreCase(givenName)) {
					loadedTable.setTable(tables.get(i).getTable());
					toReturn.setTable(tables.get(i).getTable());
					// return changeToObject(loadedTable);
				}
			}
		} else {
			boolean found = false;
			for (int i = 0; i < tables.size(); i++) {
				if (tables.get(i).getName().equalsIgnoreCase(givenName)) {
					loadedTable.setTable(tables.get(i).getTable());
					found = true;
				}
			}
			if (found) {
				ArrayList<String> colsFound = getColName(loadedTable);
				for (int i = 0; i < colsFound.size(); i++) {
					String temp = colsFound.get(i);
					temp = temp.toLowerCase();
				}
				ArrayList<String> neededCols = new ArrayList();
				for (int i = 1; i < partsOfQuery.length - 2; i++) {
					if (colsFound.contains(partsOfQuery[i].toLowerCase())) {
						neededCols.add(partsOfQuery[i].toLowerCase());
					} else {
						throw new SQLException(givenName);
					}
				}

				Table newOne = new Table();
				ArrayList<Column> colsToReturn = new ArrayList();
				ArrayList<Column> foundCols = loadedTable.getTable();
				for (int i = 0; i < foundCols.size(); i++) {
					if (neededCols.contains(foundCols.get(i).getName())) {
						colsToReturn.add(foundCols.get(i));
					}
				}
				ArrayList<Column> sortedCol = new ArrayList();
				for (int i = 0; i < neededCols.size(); i++) {
					int index = -1;
					for (int j = 0; j < foundCols.size(); j++) {
						Column col = foundCols.get(j);
						if (col.getName().equals(neededCols.get(i))) {
							sortedCol.add(col);
						}
					}
				}
				newOne.setTable(sortedCol);
				toReturn.setTable(sortedCol);
				// return changeToObject(newOne);
			} else {
				throw new SQLException(givenName);
			}
		}

		if (query.contains("where")) {
			String condition1 = query.substring(query.indexOf("where") + 6);
			String sign = "=";
			if (condition1.contains("=")) {
				sign = "=";
			} else if (condition1.contains(">")) {
				sign = ">";
			} else if (condition1.contains("<")) {
				sign = "<";
			}

			String condition = partsOfQuery[partsOfQuery.length - 1];
			String[] conditionArray = { partsOfQuery[partsOfQuery.length - 2], sign,
					partsOfQuery[partsOfQuery.length - 1] };
			conditionArray[0] = conditionArray[0].toLowerCase();
			conditionArray[2] = conditionArray[2].toLowerCase();
			ArrayList<String> colFound = getColName(loadedTable);
			int index = colFound.indexOf(conditionArray[0].toLowerCase());
			ArrayList<Integer> myIndices = indexToUpdate(conditionArray, changeToObject(loadedTable), index);
			if (myIndices.isEmpty()) {
				return new Object[0][0];
			}
			Object[][] realTable = changeToObject(loadedTable);
			Object[][] newArray = new Object[myIndices.size()][realTable[0].length];
			int j = 0;
			for (int i = 0; i < realTable.length; i++) {
				boolean f = false;
				for (int k = 0; !f && k < myIndices.size(); k++) {
					if (i == myIndices.get(k)) {
						f = true;
					}
				}
				if (f) {
					for (int y = 0; y < realTable[0].length; y++) {
						newArray[j][y] = realTable[i][y];
					}
					j++;
				}
			}
			return newArray;

		} else {

			return changeToObject(toReturn);
		}
	}

	@Override
	public int executeUpdateQuery(String query) throws SQLException {
		query = query.trim();
		query = query.toLowerCase();
		int returned = 0;
		if (query.toLowerCase().contains("insert")) {
			returned = insert(query);
		}
		if (query.toLowerCase().contains("delete")) {
			returned = delete(query);
		}
		if (query.toLowerCase().contains("update")) {
			returned = update(query);
		}
		return returned;
	}

	public int update(String query) throws SQLException {
		int toReturn = 0;
		Table loadedTable = new Table();
		query = query.trim();
		if (currentDatabase == null) {
			return 0;
		}
		String condition1 = query.substring(query.indexOf("where") + 6);
		String sign = "=";
		if (condition1.contains("=")) {
			sign = "=";
		} else if (condition1.contains(">")) {
			sign = ">";
		} else if (condition1.contains("<")) {
			sign = "<";
		}
		String[] partsOfQuery = query.split("\\W+");
		String givenName = partsOfQuery[1];
		File myDatabase = new File(Separator + "Databases" + Separator + currentDatabase);
		boolean found = false;
		ArrayList<Table> tables = currentdb.getDatabase();
		for (int i = 0; i < tables.size(); i++) {
			if (tables.get(i).getName().equalsIgnoreCase(givenName)) {
				loadedTable.setTable(tables.get(i).getTable());
				loadedTable.setName(givenName);
				;
				found = true;
			}
		}
		if (!found) {
			throw new SQLException();
		}
		String lowerCasedQuery = query.toLowerCase();
		if (lowerCasedQuery.contains("where")) {
			int startIndex = lowerCasedQuery.indexOf("set") + 3;
			int endIndex = lowerCasedQuery.indexOf("where") - 1;
			String newValues = query.substring(startIndex, endIndex);
			String[] splittedNewValues = newValues.trim().split(",");
			String[][] splitted2D = new String[splittedNewValues.length][2];
			for (int i = 0; i < splittedNewValues.length; i++) {
				splittedNewValues[i] = splittedNewValues[i].trim();
				splitted2D[i] = splittedNewValues[i].split("\\W+");
			}
			ArrayList<String> colFound = getColName(loadedTable);
			for (int i = 0; i < colFound.size(); i++) {
				String temp = colFound.get(i);
				temp = temp.toLowerCase();
			}
			String condition = partsOfQuery[partsOfQuery.length - 1];
			String[] conditionArray = { partsOfQuery[partsOfQuery.length - 2], sign,
					partsOfQuery[partsOfQuery.length - 1] };
			conditionArray[0] = conditionArray[0].toLowerCase();
			conditionArray[2] = conditionArray[2].toLowerCase();
			for (int i = 0; i < splittedNewValues.length; i++) {
				if (!colFound.contains(splitted2D[i][0].toLowerCase())) {
					try {
						throw new SQLException();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (!colFound.contains(conditionArray[0].toLowerCase())) {
				try {
					throw new SQLException();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int indexToUpdate = 0;
			ArrayList<Column> myCols = loadedTable.getTable();
			int index = colFound.indexOf(conditionArray[0].toLowerCase());
			ArrayList<Integer> myIndices = indexToUpdate(conditionArray, changeToObject(loadedTable), index);
			if (myIndices.isEmpty()) {
				try {
					throw new SQLException();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			for (int k = 0; k < myCols.size(); k++) {
				Column currentCol = myCols.get(k);
				String addedVal = "";
				boolean found3 = false;
				for (int f = 0; !found3 && f < splitted2D.length; f++) {
					if (splitted2D[f][0].equalsIgnoreCase(currentCol.getName())) {
						found3 = true;
						addedVal = splitted2D[f][1];
					}
				}
				toReturn = myIndices.size();
				ArrayList<Object> Data = currentCol.getColumn();
				for (int u = 0; u < myIndices.size(); u++) {
					if (currentCol.getType().equals("int")) {
						int itm = Integer.valueOf(addedVal + "");
						Data.set(myIndices.get(u), itm);
					} else {
						Data.set(myIndices.get(u), addedVal);
					}
				}

			}
			loadedTable.setTable(myCols);
		} else {
			String vals = query.substring(lowerCasedQuery.indexOf("set") + 3);
			vals = vals.trim();
			String[] conds = vals.split(",");
			toReturn = conds.length;
			String[][] splitted2D = new String[conds.length][2];
			for (int i = 0; i < conds.length; i++) {
				splitted2D[i] = conds[i].split("\\W+");
			}
			ArrayList<Column> myCols = loadedTable.getTable();
			Column currentCol = new Column();
			for (int i = 0; i < myCols.size(); i++) {
				currentCol = myCols.get(i);
				for (int j = 0; j < conds.length; j++) {
					if (currentCol.getName().equalsIgnoreCase(splitted2D[j][0])) {
						ArrayList<Object> myData = currentCol.getColumn();
						toReturn = myData.size();
						if (currentCol.getType().equals("varchar")) {
							int num = myData.size();
							myData.clear();
							for (int k = 0; k < num; k++) {
								myData.add(splitted2D[j][1]);
							}
						} else {
							toReturn = myData.size();
							if (splitted2D[j][1].matches("[0-9]+")) {
								int num = myData.size();
								myData.clear();
								for (int k = 0; k < num; k++) {
									int itm = Integer.valueOf(splitted2D[j][1] + "");
									myData.add(itm);
								}

							}
						}
					}
				}
			}
			loadedTable.setTable(myCols);

		}
		/*
		 * saveXMLFile(loadedTable); saveDTDFile(loadedTable);
		 */
		return toReturn;
	}

	public int delete(String query) {
		int toReturn = 1;
		Table loadedTable = new Table();
		query = query.trim();
		if (currentDatabase == null) {
			return 0;
		}
		String condition1 = query.substring(query.indexOf("where") + 6);
		String sign = "=";
		if (condition1.contains("=")) {
			sign = "=";
		} else if (condition1.contains(">")) {
			sign = ">";
		} else if (condition1.contains("<")) {
			sign = "<";
		}
		String[] partsOfQuery = query.split("\\W+");
		String givenName = partsOfQuery[2];
		File myDatabase = new File(Separator + "Databases" + Separator + currentDatabase);

		boolean found = false;
		ArrayList<Table> tables = currentdb.getDatabase();
		for (int i = 0; i < tables.size(); i++) {
			if (tables.get(i).getName().equalsIgnoreCase(givenName)) {
				loadedTable.setTable(tables.get(i).getTable());
				loadedTable.setName(givenName);
				;
				found = true;
			}
		}
		if (!found) {
			return 0;
		}

		String lowerCasedQuery = query.toLowerCase();
		if (!lowerCasedQuery.contains("where")) {
			ArrayList<Column> myCols = loadedTable.getTable();
			for (int i = 0; i < myCols.size(); i++) {
				Column currentCol = myCols.get(i);
				ArrayList<Object> myCol = currentCol.getColumn();
				toReturn = myCol.size();
				myCol.clear();
				loadedTable.setTable(myCols);
			}
		} else {
			String condition = query.substring(lowerCasedQuery.indexOf("where") + 5);
			condition = condition.trim();
			String splittedInput[] = condition.split("\\W+");
			String[] conditionArray = { partsOfQuery[partsOfQuery.length - 2], sign,
					partsOfQuery[partsOfQuery.length - 1] };
			conditionArray[0] = conditionArray[0].toLowerCase();
			conditionArray[2] = conditionArray[2].toLowerCase();
			ArrayList<String> colFound = getColName(loadedTable);
			int index = colFound.indexOf(conditionArray[0].toLowerCase());
			ArrayList<Integer> myIndices = indexToUpdate(conditionArray, changeToObject(loadedTable), index);
			toReturn = myIndices.size();
			if (myIndices.size() == 0) {
				return 0;
			}
			for (int i = 0; i < colFound.size(); i++) {
				String temp = colFound.get(i);
				temp = temp.toLowerCase();
			}
			if (!colFound.contains(splittedInput[0].toLowerCase())) {
				return 0;
			}
			ArrayList<Column> myCol = loadedTable.getTable();
			boolean found2 = false;
			int indexToDelete = 0;
			for (int i = 0; i < myCol.size() && !found2; i++) {
				Column currentCol = myCol.get(i);
				ArrayList<Object> myData = currentCol.getColumn();
				Object toDelete = myData.get(myIndices.get(0));
				for (int j = 0; j < myIndices.size(); j++) {
					myData.remove(toDelete);
				}
			}
			loadedTable.setTable(myCol);
		}
		/*
		 * saveXMLFile(loadedTable); saveDTDFile(loadedTable);
		 */

		return toReturn;
	}

	public int insert(String query) throws SQLException {
		int toReturn = 1;
		Table loadedTable = new Table();
		query = query.trim();
		if (currentdb == null) {
			throw new SQLException(query);
		}
		String[] partsOfQuery = query.split("\\W+");
		String givenName = partsOfQuery[2];
		File myDatabase = new File(Separator + "Databases" + Separator + currentDatabase);

		boolean found = false;
		ArrayList<Table> tables = currentdb.getDatabase();
		for (int i = 0; i < tables.size(); i++) {
			if (tables.get(i).getName().equalsIgnoreCase(givenName)) {
				loadedTable.setTable(tables.get(i).getTable());
				loadedTable.setName(givenName);
				;
				found = true;
			}
		}
		if (!found) {
			return 0;
		}
		// 2 case
		if (partsOfQuery[3].toLowerCase().equals("values")) {
			if (partsOfQuery.length - 4 != loadedTable.getTable().size()) {
				throw new SQLException(query);
			} else {
				ArrayList<Column> myCols = loadedTable.getTable();
				Column currentCol = new Column();
				toReturn = 1;
				for (int i = 0; i < loadedTable.getTable().size(); i++) {
					currentCol = myCols.get(i);
					ArrayList<Object> myData = currentCol.getColumn();
					if (currentCol.getType().equals("varchar")) {
						myData.add(partsOfQuery[i + 4] + "");
					} else {
						if (partsOfQuery[i + 4].matches("[0-9]+")) {
							int itm = Integer.valueOf(partsOfQuery[i + 4] + "");
							myData.add(itm);
						} else {
							throw new SQLException(query);
						}
					}
				}
			}
		} else {
			String lowerCasedQuery = query.toLowerCase();
			int firstIndex;
			try {
				firstIndex = lowerCasedQuery.indexOf(loadedTable.getName().toLowerCase())
						+ loadedTable.getName().length();
			} catch (NullPointerException e) {
				throw new NullPointerException(loadedTable.getName());
			}
			int endIndex = lowerCasedQuery.indexOf("values") - 1;
			String inputs = query.substring(firstIndex, endIndex);
			inputs = inputs.trim();
			inputs = inputs.replaceAll("[()]", "");
			String[] inputSplitted = inputs.split("\\W+");

			String values = query.substring(endIndex + 7, query.length());
			values = values.trim();
			values = values.replaceAll("[()]", "");
			values = values.replaceAll("['']", "");
			String[] givenValue = values.split("\\W+");

			// le7ad hena ana m3aya el ana 3ayzo values w columns
			ArrayList<String> foundCols = getColName(loadedTable);
			toReturn = 1;
			for (int i = 0; i < foundCols.size(); i++) {
				String temp = foundCols.get(i);
				temp = temp.toLowerCase();
			}
			for (int i = 0; i < inputSplitted.length; i++) {
				if (!foundCols.contains(inputSplitted[i].toLowerCase())) {
					throw new SQLException(query);
				}
			}
			ArrayList<Column> myCols = loadedTable.getTable();
			Column currentCol = new Column();
			for (int i = 0; i < myCols.size(); i++) {
				currentCol = myCols.get(i);
				ArrayList<Object> myData = currentCol.getColumn();
				boolean found1 = false;
				for (int j = 0; j < inputSplitted.length; j++) {
					if (currentCol.getName().equalsIgnoreCase(inputSplitted[j])) {
						if (currentCol.getType().equals("varchar")) {
							myData.add(givenValue[j]);
							found1 = true;
						} else {
							if (givenValue[j].matches("[0-9]+")) {
								int itm = Integer.valueOf(givenValue[j] + "");
								myData.add(itm);
								found1 = true;
							} else {
								return 0;
							}
						}
					}
				}
				if (!found1) {
					myData.add(null);
				}
				found1 = false;
			}
		}
		/*
		 * saveXMLFile(loadedTable); saveDTDFile(loadedTable);
		 */
		return toReturn;
	}

	public ArrayList<Integer> indexToUpdate(String[] condition, Object[][] table, int index) {
		ArrayList<Integer> myIndices = new ArrayList();
		for (int i = 0; i < table.length; i++) {
			if (condition[1] == "=") {
				String t = table[i][index] + "";
				if (t.equalsIgnoreCase(condition[2])) {
					myIndices.add(i);
				}
			} else if (condition[1] == ">") {
				String temp = table[i][index] + "";
				int first = Integer.parseInt(temp);
				int second = Integer.parseInt(condition[2]);
				if (first > second) {
					myIndices.add(i);
				}
			} else if (condition[1] == "<") {
				String temp = table[i][index] + "";
				int first = Integer.parseInt(temp);
				int second = Integer.parseInt(condition[2]);
				if (first < second) {
					myIndices.add(i);
				}
			}
		}
		return myIndices;
	}

	public Table makeTable(String query) {
		Table table = new Table();
		ArrayList<Column> cols = new ArrayList<Column>();
		int startIndex = query.indexOf('(');
		int endIndex = -1;
		endIndex = query.lastIndexOf(')');
		query = query.substring(startIndex + 1, endIndex);
		String[] parts = query.split(",");
		int size = parts.length;
		for (int i = 0; i < size; i++) {
			String field = parts[i];
			field = field.trim();
			String[] fieldInfo = field.split("\\s+");
			Column c = new Column();
			c.setName(fieldInfo[0].trim());
			c.setType(fieldInfo[1].trim());
			ArrayList<Object> data = new ArrayList();
			c.setColumn(data);
			cols.add(c);
		}
		table.setTable(cols);
		return table;
	}

	public ArrayList<String> getColName(Table table) {
		ArrayList<String> toReturn = new ArrayList();
		ArrayList<Column> myCols = table.getTable();
		for (int i = 0; i < myCols.size(); i++) {
			toReturn.add(myCols.get(i).getName());
		}
		return toReturn;
	}

	public Table loadXMLFile(String fileName) {
		Table ret = new Table();
		String path = Separator + "Databases" + Separator + currentDatabase + Separator + fileName;
		File p = new File(path + ".xml");
		InputStream inputStream = null;
		Reader reader = null;
		try {
			inputStream = new FileInputStream(p);
			try {
				reader = new InputStreamReader(inputStream, "ISO-8859-1");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		InputSource is = new InputSource(reader);
		is.setEncoding("ISO-8859-1");

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document myFile = db.parse(is);
			Node tableName = myFile.getFirstChild();
			Element main = myFile.getDocumentElement();
			System.out.println(main.getTextContent());
			NodeList members = main.getElementsByTagName("Member");
			NodeList myList = members.item(0).getChildNodes();
			System.out.println(members.getLength());
			ArrayList<Column> table = new ArrayList();
			for (int i = 0; i < myList.getLength(); i++) {
				Column newCol = new Column();
				ArrayList<Object> newAL = new ArrayList();
				Element current = (Element) myList.item(i);
				newCol.setName(current.getTagName());
				newCol.setType(current.getAttribute("Type"));
				if (!current.getTextContent().equals(" ")) {
					if (current.getTextContent().matches("[0-9]+")) {
						int itm = Integer.valueOf(current.getTextContent() + "");
						newAL.add(itm);
					} else {
						newAL.add(current.getTextContent() + "");
					}
				} else {
					// newCol.setColumn(newAL);
				}
				newCol.setColumn(newAL);
				table.add(newCol);
			}
			for (int i = 1; i < members.getLength(); i++) {
				Element current = (Element) members.item(i);
				NodeList content = current.getChildNodes();
				for (int j = 0; j < content.getLength(); j++) {
					Column toAddInside = table.get(j);
					ArrayList<Object> toADD = toAddInside.getColumn();
					toADD.add(content.item(j).getTextContent());

				}
			}
			ret = new Table();
			ret.setName(fileName);
			ret.setTable(table);
			inputStream.close();

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public void saveDTDFile(Table table) {
		String fileName = table.getName() + ".dtd";
		File f = new File(currentDatabase + Separator + table.getName() + ".dtd");
		if (f.exists()) {
			f.delete();
		}
		try {
			FileWriter fw = new FileWriter(currentDatabase + Separator + table.getName() + ".dtd");
			PrintWriter writer = new PrintWriter(fw);
			writer.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			writer.println("<!ELEMENT " + table.getName() + " (Member)+>");
			ArrayList<Column> tablee = table.getTable();
			ArrayList<String> names = new ArrayList();
			String format = new String();
			format = "(";
			for (int i = 0; i < tablee.size(); i++) {
				Column current = tablee.get(i);
				names.add(current.getName());
				format += current.getName() + ",";
			}
			format = format.substring(0, format.length() - 1);
			format += ')';
			writer.println("<!ELEMENT Member " + format + ">");
			for (int i = 0; i < names.size(); i++) {
				writer.println("<!ELEMENT " + names.get(i) + " (#PCDATA)>");
				writer.println("<!ATTLIST " + names.get(i) + " type (varchar | int) \"varchar\">");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void saveXMLFile(Table table) {
		String fileName = table.getName() + ".xml";
		String path = currentdb.getName() + Separator + table.getName() + ".xml";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document myFile = db.newDocument();
			dbf.setValidating(false); // s
			Element main = myFile.createElement(fileName.substring(0, fileName.length() - 4));
			myFile.appendChild(main);
			ArrayList<Column> myColList = table.getTable();
			Column first = myColList.get(0);
			if (first.getColumn().size() == 0) {
				Element member = myFile.createElement("Member");
				for (int i = 0; i < myColList.size(); i++) {
					Column currentCol = myColList.get(i);
					Element toBeAdded = myFile.createElement(currentCol.getName());
					toBeAdded.setAttribute("Type", currentCol.getType());
					toBeAdded.appendChild(myFile.createTextNode(" "));
					member.appendChild(toBeAdded);
					main.appendChild(member);
				}

			} else {
				ArrayList<Object> dataOfFirst = first.getColumn();
				for (int j = 0; j < dataOfFirst.size(); j++) {
					Element member = myFile.createElement("Member");
					for (int i = 0; i < myColList.size(); i++) {
						Column currentCol = myColList.get(i);
						ArrayList<Object> myData = currentCol.getColumn();
						Element toBeAdded = myFile.createElement(currentCol.getName());
						toBeAdded.setAttribute("Type", currentCol.getType());
						toBeAdded.appendChild(myFile.createTextNode(myData.get(j) + ""));
						member.appendChild(toBeAdded);
						main.appendChild(member);

					}
				}
			}
			// end of saving
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			// transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
			// table.getName() +
			// ".dtd");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			DOMSource source = new DOMSource(myFile);
			FileOutputStream fos = new FileOutputStream(path);
			StreamResult file = new StreamResult(fos);
			transformer.transform(source, file);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Object[][] changeToObject(Table table) {
		ArrayList<Column> Iterator = table.getTable();
		Column col = Iterator.get(0);
		ArrayList<Object> items = col.getColumn();
		Object[][] objectVersionOfTable = new Object[items.size()][Iterator.size()];
		for (int j = 0; j < Iterator.size(); j++) {
			for (int i = 0; i < items.size(); i++) {
				col = Iterator.get(j);
				items = col.getColumn();
				if (col.getType().equals("int")) {
					int itm = Integer.valueOf(items.get(i) + "");
					objectVersionOfTable[i][j] = itm;
				} else {
					objectVersionOfTable[i][j] = items.get(i) + "";

				}
			}
		}
		return objectVersionOfTable;
	}

	public Table changeToOTable(Object[][] ObjectVersionOfTable) {
		Table changedToTable = new Table();
		ArrayList<Column> Table = new ArrayList();
		Column col = new Column();
		for (int j = 0; j < ObjectVersionOfTable[0].length; j++) {
			ArrayList<Object> items = new ArrayList();
			col = new Column();
			for (int i = 0; i < ObjectVersionOfTable.length; i++) {
				items.add(ObjectVersionOfTable[i][j]);
			}
			col.setColumn(items);
			Table.add(col);
		}
		changedToTable.setTable(Table);
		return changedToTable;
	}
}
