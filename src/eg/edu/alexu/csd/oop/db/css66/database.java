package eg.edu.alexu.csd.oop.db.css66;

import java.util.ArrayList;

public class database{
	
	private String name=null;
	private ArrayList<Table> database=null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Table> getDatabase() {
		return database;
	}
	public void setDatabase(ArrayList<Table> database) {
		this.database = database;
	}
	public void addTable(Table t){
		this.database.add(t);
	}
	
}
