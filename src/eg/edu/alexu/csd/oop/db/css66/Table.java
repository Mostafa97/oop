package eg.edu.alexu.csd.oop.db.css66;

import java.util.ArrayList;

public class Table {
	private ArrayList<Column> table;
	private String name;
	private String Path;
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getPath()
	{
		return Path;
	}
	
	public void setPath(String name)
	{
		this.Path = name;
	}
	
	public ArrayList<Column> getTable()
	{
		return table;
	}
	
	public void setTable(ArrayList<Column> table)
	{
		this.table = table;
	}
}
