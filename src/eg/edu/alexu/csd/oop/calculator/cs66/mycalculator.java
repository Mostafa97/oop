package eg.edu.alexu.csd.oop.calculator.cs66;

import eg.edu.alexu.csd.oop.calculator.Calculator;
import java.util.*;
import java.io.*;

public class mycalculator implements Calculator {
	private String current = null;
	private int ptr;
	private int savedptr = -1;
	LinkedList<String> history;

	public mycalculator() {
		history = new LinkedList<String>();
		save();

	}

	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		if (history.size() == 5) {
			history.removeFirst();
		}

		current = s;
		history.add(current);
		ptr = history.size() - 1;

	}

	@Override
	public String getResult() {
		// TODO Auto-generated method stub
		String first = "";
		String second = "";
		char sign;
		int i = 0;

		while (i < current.length()
				&& ((current.charAt(i) >= '0' && current.charAt(i) <= '9') || current.charAt(i) == '.')) {
			first += current.charAt(i);
			i++;

		}

		if (i < current.length()) {
			sign = current.charAt(i);
			i++;
		} else {
			throw null;
		}

		while (i < current.length()
				&& ((current.charAt(i) >= '0' && current.charAt(i) <= '9') || current.charAt(i) == '.')) {
			second += current.charAt(i);
			i++;

		}
		if (i != current.length() || first == "" || second == "") {
			throw null;
		}
		try {
			Double.parseDouble(first);
		} catch (NumberFormatException e) {
			throw null;
		}
		try {
			Double.parseDouble(second);
		} catch (NumberFormatException e) {
			throw null;
		}
		double op1 = Double.parseDouble(first);
		double op2 = Double.parseDouble(second);
		double res;
		switch (sign) {
		case '+':
			res = op1 + op2;
			break;
		case '-':
			res = op1 - op2;
			break;
		case '*':
			res = op1 * op2;
			break;
		case '/':
			if (op2 != 0) {
				res = op1 / op2;
			} else {
				throw null;
			}
			break;
		default:
			throw null;

		}

		return String.valueOf(res);
	}

	@Override
	public String current() {

		return current;
	}

	@Override
	public String prev() {
		// TODO Auto-generated method stub
		if (ptr > 0) {
			ptr--;
			current = history.get(ptr);

			return current;
		} else {
			return null;
		}

	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		if (ptr < history.size() - 1) {
			ptr++;
			current = history.get(ptr);

			return current;
		} else {
			return null;
		}

	}

	@Override
	public void save() {
		// TODO Auto-generated method stub
		try {
			savedptr = ptr;
			FileWriter his = new FileWriter("history.txt");

			PrintWriter write = new PrintWriter(his);

			for (int i = 0; i < history.size(); i++) {
				write.println(history.get(i));
			}
			write.close();
		} catch (Exception e) {
			throw null;
		}

	}

	@Override
	public void load() {
		// TODO Auto-generated method stub

		try {
			FileReader fr = new FileReader("history.txt");
			BufferedReader br = new BufferedReader(fr);
			String x;
			history.clear();

			while ((x = br.readLine()) != null) {
				history.add(x);

			}
			if (history.size() != 0) {
				ptr = savedptr;
				current = history.get(ptr);

			} else {
				current = null;
				ptr = -1;
			}

		} catch (Exception e) {
			throw null;
		}

	}

}
