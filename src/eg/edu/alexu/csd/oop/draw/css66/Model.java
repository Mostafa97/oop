package eg.edu.alexu.csd.oop.draw.css66;



import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
//import java.awt.Shape;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Model extends drawEngine {
	int Indicator=-1;
	Shape Guide;
	Color strokeColor = Color.BLACK;
	Color fillColor = Color.LIGHT_GRAY;
	public Model(){
		ptr=0;
		LinkedList start=new LinkedList();
		memento first=new memento(start);
		previous.add(first);
		
	}
	public void paint(Graphics g){
		Graphics2D canvas = (Graphics2D)g;
		canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		canvas.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
		//draw all saved shapes
		refresh(canvas);
		//check if there is a guide shape to be drawn
		
		if(Guide!=null){
			canvas.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.40f));
			Guide.draw(canvas);
			Guide=null;
		}
		
	}
	public void plugIn(String path){
		if(path.endsWith(".jar")){
			try {
				JarFile jar =new JarFile(path);
				Enumeration <JarEntry> list=jar.entries();
				URL[] urls={new URL("jar:file:"+path+"!/")};
				URLClassLoader cls =new URLClassLoader(urls);
				while(list.hasMoreElements()){
					JarEntry en =list.nextElement();
					if(en.getName().endsWith(".class")){
						String classPath=en.getName().substring(0,en.getName().length()-6).replace("/", ".");
						Class x;
						try {
							x = cls.loadClass(classPath);
							if(Shape.class.isAssignableFrom(x)){
								supportedShapes.add(supportedShapes.size(), x);
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void setguide(Shape guide){
		Guide=guide;
	}
	
	public Map rectangleprop(double x1,double y1,double x2,double y2){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 1.0);
		  prop.put("id",(double)noShapes );
		  prop.put("PostionX",Math.min(x1, x2));
		  prop.put("PostionY",Math.min(y1, y2));
		  prop.put("Width", Math.abs((double)(x1-x2)));
		  prop.put("Length", Math.abs((double)(y1-y2)));
	        prop.put("ArcWidth",0.0);
	        prop.put("ArcLength", 0.0);
		  return prop;
	  }
	
	public Map squareProp(double x1,double y1,double x2,double y2){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 2.0);
		  prop.put("id",(double)noShapes );
		  prop.put("PostionX",Math.min(x1,x2));
		  
		  prop.put("Side", Math.abs((double)(x1-x2)));
		  if(y2<y1){
			  prop.put("PostionY",y1-prop.get("Side"));
			  
		  }else{
			  prop.put("PostionY",y1);
		  }
		  return prop;
	  }
	
	public Map circleProp(double x1,double y1,double x2,double y2){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 6.0);
		  prop.put("id",(double)noShapes );
		  prop.put("CenterX", x1);
		  prop.put("CenterY",y1);
		  prop.put("Radius",(double)Math.round(Math.hypot(x1-x2, y1-y2)*100)/100);
		  return prop;
	  }
	
	public Map ellipseProp(double x1,double y1,double x2,double y2){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 5.0);
		  prop.put("id",(double)noShapes );
		  prop.put("CenterX", x1);
		  prop.put("CenterY",y1);
		  prop.put("Width", 2.0*Math.abs((double)(x1-x2)));
		  prop.put("Height", 2.0*Math.abs((double)(y1-y2)));
		  return prop;
	  }
	public Map lineProp(double x1,double y1,double x2,double y2){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 4.0);
		  prop.put("id",(double)noShapes );
		  prop.put("startX", x1);
		  prop.put("startY", y1);
		  prop.put("endX", x2);
		  prop.put("endY", y2);
		  return prop;
	  }
	public Map triProp(double x1,double y1,double x2,double y2,double x3,double y3){
		  Map<String, Double> prop;
		  prop = new HashMap<>();
		  prop.put("Name", 3.0);
		  prop.put("id",(double)noShapes );
		  prop.put("firstX", x1);
		  prop.put("firstY", y1);
		  prop.put("secondX", x2);
		  prop.put("secondY", y2);
		  prop.put("thirdX", x3);
		  prop.put("thirdY", y3);
		  return prop;
	  }
	
}

