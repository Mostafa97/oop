package eg.edu.alexu.csd.oop.draw.css66;



import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;



public class View extends JFrame {
	
	
	JPanel south;
	//south buttons
	private JButton Rectangle,Triangle,LineSegment,Square,Circle,Ellipse,Stroke,Filler,Undo,Redo,Save,Load;
	
	
	Box east;
	//east panels
	JLabel labelExternal;
	 JComboBox externalShapes;
	JPanel first,dynamicPanel,updatePanel,last;
	
	private JButton update,plugIn,selectShape,copy;
	LinkedList <JLabel> propLabels;
	LinkedList <JTextArea> propTextArea;
	drawingBoard board;
	Model model;
	JLabel yourShapes;
	JComboBox itemList;
	JButton remove;
	
	public View(Model mode)
	{
		
		model=mode;
		this.setVisible(true);
		this.setSize(2500,1500);
		this.setTitle("draw");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout(0, 0));
		//set south layout
		
		south=new JPanel();
		//Rectangle Starts
		Rectangle=new JButton();
		Rectangle.setPreferredSize(new Dimension(150, 150));
		Rectangle.setFont(new Font("Tahoma", Font.BOLD, 34));
		Rectangle.setBackground(Color.white);
		Icon rect = new ImageIcon("./images/rec.png");
		Rectangle.setIcon(rect);
		//Rectangle Ends
		//Sqaure starts
		Square=new JButton();
		Square.setPreferredSize(new Dimension(150, 150));
		Square.setFont(new Font("Tahoma", Font.BOLD, 34));
		Square.setBackground(Color.white);
		Icon sqr = new ImageIcon("./images/sqr.png");
		Square.setIcon(sqr);
		//square ends
		//circle starts
		Circle=new JButton();
		Circle.setPreferredSize(new Dimension(150, 150));
		Circle.setFont(new Font("Tahoma", Font.BOLD, 34));
		Circle.setBackground(Color.white);
		Icon cir = new ImageIcon("./images/cir.png");
		Circle.setIcon(cir);
		//circle end
		//ellipse starts
		Ellipse=new JButton();
		Ellipse.setPreferredSize(new Dimension(150, 150));
		Ellipse.setFont(new Font("Tahoma", Font.BOLD, 34));
		Ellipse.setBackground(Color.white);
		Icon ell = new ImageIcon("./images/ellipse.png");
		Ellipse.setIcon(ell);
		//ellipse ends
		//Triangle starts
		Triangle=new JButton();
		Triangle.setPreferredSize(new Dimension(150, 150));
		Triangle.setFont(new Font("Tahoma", Font.BOLD, 34));
		Triangle.setBackground(Color.white);
		Icon tr = new ImageIcon("./images/tri.png");
		Triangle.setIcon(tr);
		//Triangle ends
		// Line starts
		LineSegment =new JButton();
		LineSegment.setPreferredSize(new Dimension(150, 150));
		LineSegment.setFont(new Font("Tahoma", Font.BOLD, 34));
		LineSegment.setBackground(Color.white);
		Icon LS = new ImageIcon("./images/line.png");
		LineSegment.setIcon(LS);
		//Line Ends
		//Fill starts
		Filler=new JButton();
		Filler.setPreferredSize(new Dimension(150, 150));
		Filler.setFont(new Font("Tahoma", Font.BOLD, 34));
		Filler.setBackground(Color.white);
		Icon fil = new ImageIcon("./images/fill.png");
		Filler.setIcon(fil);
		//fill ends
		//stroke starts
		Stroke=new JButton();
		Stroke.setPreferredSize(new Dimension(150, 150));
		Stroke.setFont(new Font("Tahoma", Font.BOLD, 34));
		Stroke.setBackground(Color.white);
		Icon str = new ImageIcon("./images/stroke.png");
		Stroke.setIcon(str);
		//stroke ends
		//undo starts
		Undo=new JButton();
		Undo.setPreferredSize(new Dimension(150, 150));
		Undo.setFont(new Font("Tahoma", Font.BOLD, 34));
		Undo.setBackground(Color.white);
		Icon und = new ImageIcon("./images/undo.png");
		Undo.setIcon(und);
		//undo ends
		//redo starts 
		Redo=new JButton();	
		Redo.setPreferredSize(new Dimension(150, 150));
		Redo.setFont(new Font("Tahoma", Font.BOLD, 34));
		Redo.setBackground(Color.white);
		Icon red = new ImageIcon("./images/redo.png");
		Redo.setIcon(red);
		//redo ends
		//save starts
				Save=new JButton();
				Save.setPreferredSize(new Dimension(150, 150));
				Save.setFont(new Font("Tahoma", Font.BOLD, 34));
				Save.setBackground(Color.white);
				Icon sav = new ImageIcon("./images/save.png");
				Save.setIcon(sav);
				//save ends
				//Load starts
				Load=new JButton();
				Load.setPreferredSize(new Dimension(150, 150));
				Load.setFont(new Font("Tahoma", Font.BOLD, 34));
				Load.setBackground(Color.white);
				Icon lod = new ImageIcon("./images/load.png");
				Load.setIcon(lod);
				//Load ends	
		south.add(Rectangle);
		south.add(Square);
		south.add(Circle);
		south.add(Ellipse);
		south.add(Triangle);
		south.add(LineSegment);
		south.add(Filler);
		south.add(Stroke);
		south.add(Redo);
		south.add(Undo);
		south.add(Save);
		south.add(Load);
		this.add(south,BorderLayout.SOUTH);
		//east layout
		copy=new JButton("Copy");
		//copy button style
			copy.setPreferredSize(new Dimension(200, 80));
			copy.setFont(new Font("Tahoma", Font.BOLD, 24));
			copy.setBackground(Color.white);
		itemList=new JComboBox();
		yourShapes=new JLabel("Your Shapes");
			//label yourShapes
				yourShapes.setPreferredSize(new Dimension(250,100));
				yourShapes.setFont(new Font("Tahoma", Font.BOLD, 24));
		// last panel 
			last=new JPanel();
			labelExternal =new JLabel("External Shape");
			//labelEcternal 
				labelExternal.setPreferredSize(new Dimension(250,100));
				labelExternal.setFont(new Font("Tahoma", Font.BOLD, 24));
			externalShapes=new JComboBox();
			selectShape=new JButton("Select Shape");
			//slectShape button
				selectShape.setPreferredSize(new Dimension(250, 100));
				selectShape.setFont(new Font("Tahoma", Font.BOLD, 24));
				selectShape.setBackground(Color.white);
			last.add(labelExternal);
			last.add(externalShapes);
			last.add(selectShape);
		//remove button
		remove=new JButton("Remove");
		remove.setPreferredSize(new Dimension(250, 100));
		remove.setFont(new Font("Tahoma", Font.BOLD, 34));
		remove.setBackground(Color.white);
		
		first = new JPanel();
		dynamicPanel = new JPanel();
		updatePanel = new JPanel();
		
		//plugin button
		plugIn = new JButton("plugIn");
		plugIn.setPreferredSize(new Dimension(250, 100));
		plugIn.setFont(new Font("Tahoma", Font.BOLD, 34));
		plugIn.setBackground(Color.white);
		
		
		update = new JButton("update");
		update.setPreferredSize(new Dimension(250, 100));
		update.setFont(new Font("Tahoma", Font.BOLD, 34));
		update.setBackground(Color.white);
		
		
		updatePanel.add(plugIn);
		updatePanel.add(update);
		updatePanel.add(remove);
		
		first.add(yourShapes);
		first.add(itemList);
		first.add(copy);
		east = Box.createVerticalBox();
		east.add(first);
		east.add(dynamicPanel);
		east.add(updatePanel);
		east.add(last);
		this.add(east,BorderLayout.WEST);
		board=new drawingBoard();
		board.repaint();
		this.add(board,BorderLayout.CENTER);
		
		
	}
	public class drawingBoard extends JComponent{
			public void paint(Graphics g){
				
				model.paint(g);
			}
	}
	
  void addMouseAdabter(MouseListener listen,MouseMotionListener listen2){
	  		
		board.addMouseListener(listen);
		board.addMouseMotionListener(listen2);
	
	}
  
  public void addUpdateEvent(ActionListener listen){
	  update.addActionListener(listen);
  }
  
  void addAL(ActionListener copyListener,ActionListener selectShapeListener,ActionListener plugInListener,ActionListener loadListener,ActionListener saveListener,ActionListener redoListener,ActionListener undoListener,ActionListener removeListener,ActionListener updateListener,ActionListener rect,ActionListener sqr,ActionListener cir,ActionListener line,ActionListener ellipse,ActionListener tri,ActionListener str,ActionListener fil,ItemListener combobox)
  {
	Rectangle.addActionListener(rect);
	Square.addActionListener(sqr);
	Circle.addActionListener(cir);
	LineSegment.addActionListener(line);
	Ellipse.addActionListener(ellipse);
	Triangle.addActionListener(tri);
	Stroke.addActionListener(str);
	Filler.addActionListener(fil);
	itemList.addItemListener(combobox);
	update.addActionListener(updateListener);
	remove.addActionListener(removeListener);
	Undo.addActionListener(undoListener);
	Redo.addActionListener(redoListener);
	Save.addActionListener(saveListener);
	Load.addActionListener(loadListener);
	plugIn.addActionListener(plugInListener);
	selectShape.addActionListener(selectShapeListener);
	copy.addActionListener(copyListener);
  }
  
  public void display(Map<String, Double> prop){
	 
	  
	  Set<String> a=prop.keySet();
	  dynamicPanel.removeAll();
	 propLabels=new LinkedList();
	 propTextArea=new LinkedList();
	  Box b=Box.createVerticalBox();
	  
	// label properties
	  	JLabel properties=new JLabel("Properties");
	  	properties.setFont(new Font("Arial Black", Font.BOLD,20));
	  
	  	properties.setAlignmentX(Component.CENTER_ALIGNMENT);
	  	b.add(properties);
	 for(String g:a){
		 JPanel j =new JPanel();
		 j.setAlignmentX(Component.CENTER_ALIGNMENT);
		 
		 
		 JLabel l=new JLabel(g);
		 JTextArea t=new JTextArea();
		 
		 
		 //adding style to label and textarea
		 l.setFont(new Font("Arial Black", Font.BOLD, 15));
		 l.setMinimumSize(new Dimension(300,300));
		 
		 t.setColumns(5);
		 t.setText(Double.toString((Double) prop.get(g)));
		 //end
		 
		 j.add(l);
		 j.add(t);
		 b.add(j);
		 propLabels.add(l);
		 propTextArea.add(t);
	 }
	 dynamicPanel.add(b);
	 dynamicPanel.revalidate(); 
  }
  
  public Map<String,Double> getdata(){
	  Map<String,Double> out=new HashMap();
	  if(propLabels.isEmpty()){
		  return null;
	  }
	  try{
	  for(int i=0;i<propLabels.size();i++){
		  String key=propLabels.get(i).getText();
		  Double Value=Double.valueOf(propTextArea.get(i).getText());
		  out.put(key, Value);
	  }
	  }
	  catch(Exception e){
		  //to be determined
		  return null;
		  //display error message*************
	  }
	  
	return out;
	  
  }
  public void removeselected(){
	  
	  
	 itemList.removeItemAt(model.selected);
	
	 model.selected=itemList.getSelectedIndex();
	
	  if(model.selected!=-1){
		  display(model.shapes.get(model.selected).getProperties());
	  }
	  if(model.shapes.isEmpty()){
		  dynamicPanel.removeAll();
	  }

	
  }
  public void Updatecombobox(int noOfShapes ){
	  
	itemList.insertItemAt("Shape"+Integer.toString(model.noShapes),noOfShapes-1);
	  
	  itemList.setSelectedIndex(model.shapes.size()-1);
	 
	  
	  
  }
  //will be used with undo and redo to refresh to the new state
  public void rebuildScreen(){
	  //update combobox
	  
	  itemList.removeAllItems();
	 
	  for(int i=0;i<model.shapes.size();i++){
		 
		 model.noShapes++;
		  itemList.addItem("shape"+(i+1));
		  
	  }
	  
	 
	 
	  //end
	  if(model.selected!=-1){
		  display(model.shapes.get(model.selected).getProperties());
	  }else{
		  dynamicPanel.removeAll();
	  }
	 
  }
		  public void updatesupportedShapes(){
			  externalShapes.removeAllItems();
			  System.out.println(model.getSupportedShapes().size());
			  int size=model.getSupportedShapes().size();
			  for(int i=0;i<size;i++){
				  externalShapes.addItem(model.getSupportedShapes().get(i).getSimpleName());
			  }
			 
			 
		  }
 
}

