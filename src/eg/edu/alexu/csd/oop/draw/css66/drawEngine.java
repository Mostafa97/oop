package eg.edu.alexu.csd.oop.draw.css66;



import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/*import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;*/
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eg.edu.alexu.csd.oop.draw.DrawingEngine;
import eg.edu.alexu.csd.oop.draw.Shape;

public class drawEngine implements DrawingEngine {
	LinkedList <Shape> shapes=new LinkedList<Shape>();
	int selected=-1;
	int noShapes=0;
	LinkedList <memento> previous=new LinkedList<memento>();
	int ptr=-1;
	int flag=0;
	LinkedList<Class<? extends Shape>> supportedShapes =new <Class<? extends Shape>> LinkedList();
	@Override
	public void refresh(Graphics canvas) {
		// TODO Auto-generated method stub
		for(Shape x:shapes){
			x.draw(canvas);
		}
		
	}

	@Override
	public void addShape(Shape shape) {
		// TODO Auto-generated method stub
		//the start state entered at start of application
		if(flag==0){
			ptr=0;
			LinkedList start=new LinkedList();
			memento first=new memento(start);
			previous.add(first);
			flag++;
		}
		shapes.add(shape);
		ptr++;
		LinkedList<Shape> newshapes =new<Shape> LinkedList();
		for(Shape x:shapes){
			newshapes.add(x);
		}
		memento state=new memento(newshapes);
		
		previous.add(ptr,state);
		
		while(previous.size()!=(ptr+1)){
			previous.removeLast();
			
		}
		
		while(previous.size()>21){
			previous.removeFirst();
			ptr=previous.size()-1;//19
		}
		
		
	}

	@Override
	public void removeShape(Shape shape) {
		// TODO Auto-generated method stub
		
		shapes.remove(shape);
		
		ptr++;
		LinkedList<Shape> newshapes =new<Shape> LinkedList();
		for(Shape x:shapes){
			newshapes.add(x);
		}
		memento state=new memento(newshapes);
		
		previous.add(ptr,state);
		
		while(previous.size()!=(ptr+1)){
			previous.removeLast();
			
		}
		
		while(previous.size()>21){
			previous.removeFirst();
			ptr=previous.size()-1;//19
		}
		
	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		// TODO Auto-generated method stub
		int i=shapes.indexOf(oldShape);
		shapes.add(i, newShape);
		shapes.remove(oldShape);
		
		ptr++;
		LinkedList<Shape> newshapes =new<Shape> LinkedList();
		for(Shape x:shapes){
			newshapes.add(x);
		}
		memento state=new memento(newshapes);
		
		previous.add(ptr,state);
		
		while(previous.size()!=(ptr+1)){
			previous.removeLast();
			
		}
		
		while(previous.size()>21){
			previous.removeFirst();
			ptr=previous.size()-1;//19
		}
		
	}

	@Override
	public Shape[] getShapes() {
		// TODO Auto-generated method stub
		Shape array[]=new Shape[shapes.size()];
		int i=0;
		for(Shape x:shapes){
			array[i]=x;
			i++;
		}
		return array;
	}

	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {
		// TODO Auto-generated method stub
		supportedShapes.add(RoundRectangle.class);
		supportedShapes.add(Square.class);
		supportedShapes.add(Triangle.class);
		supportedShapes.add(Circle.class);
		supportedShapes.add(Ellipse.class);
		supportedShapes.add(Line.class);
		return supportedShapes;
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		
		if(ptr>=1){
			
			ptr--;
			memento prevState=previous.get(ptr);
			shapes=new LinkedList<Shape>();
			
			LinkedList<Shape> oldstate=prevState.getShapes();
			for(Shape x:oldstate){
				shapes.add(x);
			}
			
		}
		
	}

	@Override
	public void redo() {
		// TODO Auto-generated method stub
		if(ptr<previous.size()-1){
			ptr++;
			memento prevState=previous.get(ptr);
			shapes=new LinkedList<Shape>();
			
			LinkedList<Shape> oldstate=prevState.getShapes();
			for(Shape x:oldstate){
				shapes.add(x);
			}
			System.out.println(shapes.size());
			
		}
	}

	@Override
	public void save(String path) {
		String PathXML = path +".xml";
		String PathJSON = path + ".json";
		if(PathXML!=null)
		{
			File f = new File(PathXML);
			Document myDocument;
			Element e = null;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			try 
			{
				DocumentBuilder db = dbf.newDocumentBuilder();
				myDocument =  db.newDocument();
				Element rootElement = myDocument.createElement("AllShapes");
				
				myDocument.appendChild(rootElement);
				rootElement.setAttribute("size", shapes.size() +""); 
				//loop 3la el shapes
				ListIterator<Shape> Iterator = shapes.listIterator();
				Shape first = shapes.getFirst();
				while(Iterator.hasNext())
				{
					first = Iterator.next();
					Map<String, Double> prop = first.getProperties();
					if(prop.get("Name") == 1.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Rectangle");
						Element NodeX = myDocument.createElement("x1");
						NodeX.appendChild(myDocument.createTextNode(first.getPosition().getX() +""));
						Element NodeY = myDocument.createElement("y1");
						NodeY.appendChild(myDocument.createTextNode(first.getPosition().getY() +""));
						Element NodeLength = myDocument.createElement("Length");
						NodeLength.appendChild(myDocument.createTextNode(prop.get("Length")+""));
						Element NodeWidth = myDocument.createElement("Width");
						NodeWidth.appendChild(myDocument.createTextNode(prop.get("Width")+""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						Element FillColor = myDocument.createElement("FillColor");
						Element NodeRedFill = myDocument.createElement("FillRed");
						NodeRedFill.appendChild(myDocument.createTextNode(first.getFillColor().getRed()+""));
						Element NodeGreenFill = myDocument.createElement("FillGreen");
						NodeGreenFill.appendChild(myDocument.createTextNode(first.getFillColor().getGreen() +""));
						Element NodeBlueFill = myDocument.createElement("FillBlue");
						NodeBlueFill.appendChild(myDocument.createTextNode(first.getFillColor().getBlue() +""));
						FillColor.appendChild(NodeRedFill);
						FillColor.appendChild(NodeGreenFill);
						FillColor.appendChild(NodeBlueFill);
						newElement.appendChild(NodeX);
						newElement.appendChild(NodeY);
						newElement.appendChild(NodeLength);
						newElement.appendChild(NodeWidth);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(FillColor);
						rootElement.appendChild(newElement);
					}
					if(prop.get("Name") == 2.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Square");
						Element NodeX = myDocument.createElement("x1");
						NodeX.appendChild(myDocument.createTextNode(first.getPosition().getX() +""));
						Element NodeY = myDocument.createElement("y1");
						NodeY.appendChild(myDocument.createTextNode(first.getPosition().getY() +""));
						Element NodeLength = myDocument.createElement("Side");
						NodeLength.appendChild(myDocument.createTextNode(prop.get("Side")+""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						Element FillColor = myDocument.createElement("FillColor");
						Element NodeRedFill = myDocument.createElement("FillRed");
						NodeRedFill.appendChild(myDocument.createTextNode(first.getFillColor().getRed()+""));
						Element NodeGreenFill = myDocument.createElement("FillGreen");
						NodeGreenFill.appendChild(myDocument.createTextNode(first.getFillColor().getGreen() +""));
						Element NodeBlueFill = myDocument.createElement("FillBlue");
						NodeBlueFill.appendChild(myDocument.createTextNode(first.getFillColor().getBlue() +""));
						FillColor.appendChild(NodeRedFill);
						FillColor.appendChild(NodeGreenFill);
						FillColor.appendChild(NodeBlueFill);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(FillColor);
						newElement.appendChild(NodeX);
						newElement.appendChild(NodeY);
						newElement.appendChild(NodeLength);
						rootElement.appendChild(newElement);
					}
					if(prop.get("Name") == 3.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Triangle");
						Element NodeX1 = myDocument.createElement("x1");
						NodeX1.appendChild(myDocument.createTextNode(prop.get("firstX") +""));
						Element NodeY1 = myDocument.createElement("y1");
						NodeY1.appendChild(myDocument.createTextNode(prop.get("firstY") +""));
						Element NodeX2 = myDocument.createElement("x2");
						NodeX2.appendChild(myDocument.createTextNode(prop.get("secondX") +""));
						Element NodeY2 = myDocument.createElement("y2");
						NodeY2.appendChild(myDocument.createTextNode(prop.get("secondY") +""));
						Element NodeX3 = myDocument.createElement("x3");
						NodeX3.appendChild(myDocument.createTextNode(prop.get("thirdX") +""));
						Element NodeY3 = myDocument.createElement("y3");
						NodeY3.appendChild(myDocument.createTextNode(prop.get("thirdY") +""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						Element FillColor = myDocument.createElement("FillColor");
						Element NodeRedFill = myDocument.createElement("FillRed");
						NodeRedFill.appendChild(myDocument.createTextNode(first.getFillColor().getRed()+""));
						Element NodeGreenFill = myDocument.createElement("FillGreen");
						NodeGreenFill.appendChild(myDocument.createTextNode(first.getFillColor().getGreen() +""));
						Element NodeBlueFill = myDocument.createElement("FillBlue");
						NodeBlueFill.appendChild(myDocument.createTextNode(first.getFillColor().getBlue() +""));
						FillColor.appendChild(NodeRedFill);
						FillColor.appendChild(NodeGreenFill);
						FillColor.appendChild(NodeBlueFill);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(FillColor);
						newElement.appendChild(NodeX1);
						newElement.appendChild(NodeY1);
						newElement.appendChild(NodeX2);
						newElement.appendChild(NodeY2);
						newElement.appendChild(NodeX3);
						newElement.appendChild(NodeY3);
						rootElement.appendChild(newElement);
					}
					if(prop.get("Name") == 4.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Line");
						Element NodeX1 = myDocument.createElement("x1");
						NodeX1.appendChild(myDocument.createTextNode(prop.get("startX") +""));
						Element NodeY1 = myDocument.createElement("y1");
						NodeY1.appendChild(myDocument.createTextNode(prop.get("startY") +""));
						Element NodeX2 = myDocument.createElement("x2");
						NodeX2.appendChild(myDocument.createTextNode(prop.get("endX") +""));
						Element NodeY2 = myDocument.createElement("y2");
						NodeY2.appendChild(myDocument.createTextNode(prop.get("endY") +""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(NodeX1);
						newElement.appendChild(NodeY1);
						newElement.appendChild(NodeX2);
						newElement.appendChild(NodeY2);
						rootElement.appendChild(newElement);
					}
					if(prop.get("Name") == 5.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Ellipse");
						Element NodeX1 = myDocument.createElement("x1");
						NodeX1.appendChild(myDocument.createTextNode(prop.get("CenterX") +""));
						Element NodeY1 = myDocument.createElement("y1");
						NodeY1.appendChild(myDocument.createTextNode(prop.get("CenterY") +""));
						Element NodeX2 = myDocument.createElement("Height");
						NodeX2.appendChild(myDocument.createTextNode(prop.get("Height") +""));
						Element NodeY2 = myDocument.createElement("Width");
						NodeY2.appendChild(myDocument.createTextNode(prop.get("Width") +""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						Element FillColor = myDocument.createElement("FillColor");
						Element NodeRedFill = myDocument.createElement("FillRed");
						NodeRedFill.appendChild(myDocument.createTextNode(first.getFillColor().getRed()+""));
						Element NodeGreenFill = myDocument.createElement("FillGreen");
						NodeGreenFill.appendChild(myDocument.createTextNode(first.getFillColor().getGreen() +""));
						Element NodeBlueFill = myDocument.createElement("FillBlue");
						NodeBlueFill.appendChild(myDocument.createTextNode(first.getFillColor().getBlue() +""));
						FillColor.appendChild(NodeRedFill);
						FillColor.appendChild(NodeGreenFill);
						FillColor.appendChild(NodeBlueFill);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(FillColor);
						newElement.appendChild(NodeX1);
						newElement.appendChild(NodeY1);
						newElement.appendChild(NodeX2);
						newElement.appendChild(NodeY2);
						rootElement.appendChild(newElement);
					}
					if(prop.get("Name") == 6.0) // y3ni rectangle
					{
						Element newElement = myDocument.createElement("Shapes");
						newElement.setAttribute("ID", "Circle");
						Element NodeX1 = myDocument.createElement("x1");
						NodeX1.appendChild(myDocument.createTextNode(prop.get("CenterX") +""));
						Element NodeY1 = myDocument.createElement("y1");
						NodeY1.appendChild(myDocument.createTextNode(prop.get("CenterY") +""));
						Element NodeX2 = myDocument.createElement("Radius");
						NodeX2.appendChild(myDocument.createTextNode(prop.get("Radius") +""));
						Element StrokeColor = myDocument.createElement("StrokeColor");
						Element NodeRed = myDocument.createElement("Red");
						NodeRed.appendChild(myDocument.createTextNode(first.getColor().getRed() +""));
						Element NodeGreen = myDocument.createElement("Green");
						NodeGreen.appendChild(myDocument.createTextNode(first.getColor().getGreen() +""));
						Element NodeBlue = myDocument.createElement("Blue");
						NodeBlue.appendChild(myDocument.createTextNode(first.getColor().getBlue() +""));
						StrokeColor.appendChild(NodeRed);
						StrokeColor.appendChild(NodeGreen);
						StrokeColor.appendChild(NodeBlue);
						Element FillColor = myDocument.createElement("FillColor");
						Element NodeRedFill = myDocument.createElement("FillRed");
						NodeRedFill.appendChild(myDocument.createTextNode(first.getFillColor().getRed()+""));
						Element NodeGreenFill = myDocument.createElement("FillGreen");
						NodeGreenFill.appendChild(myDocument.createTextNode(first.getFillColor().getGreen() +""));
						Element NodeBlueFill = myDocument.createElement("FillBlue");
						NodeBlueFill.appendChild(myDocument.createTextNode(first.getFillColor().getBlue() +""));
						FillColor.appendChild(NodeRedFill);
						FillColor.appendChild(NodeGreenFill);
						FillColor.appendChild(NodeBlueFill);
						newElement.appendChild(StrokeColor);
						newElement.appendChild(FillColor);
						newElement.appendChild(NodeX1);
						newElement.appendChild(NodeY1);
						newElement.appendChild(NodeX2);
						rootElement.appendChild(newElement);
					}
					
				}
				//end of loop
				
		       
		        try {
		            Transformer tr = TransformerFactory.newInstance().newTransformer();
		        //    tr.setOutputProperty(OutputKeys.INDENT, "yes");
		            tr.setOutputProperty(OutputKeys.METHOD, "xml");
		            tr.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
		          //  tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		            // send DOM to file
		            tr.transform(new DOMSource(myDocument),new StreamResult(new FileOutputStream(f)));

		        } catch (TransformerException te) {
		            System.out.println(te.getMessage());
		        } catch (IOException ioe) {
		            System.out.println(ioe.getMessage());
		        }
			}
			catch(ParserConfigurationException pce)
			{
				System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
			}
		}
		
		
		//JSON IMPLEMENTATION STARTS HERE
		/*if(PathJSON != null)
		{
			ListIterator<Shape> Iterator = shapes.listIterator();
			Shape first = shapes.getFirst();
			JSONObject myObject = new JSONObject();  
			JSONArray myArray;
			int counter = 0;
			myObject.put("Size", shapes.size()+"");
			while(Iterator.hasNext())
			{
				myArray = new JSONArray();
				first = Iterator.next();
				Map<String, Double> prop = first.getProperties();
				if(prop.get("Name") == 1.0) // y3ni rectangle
				{
					String x1 = prop.get("PostionX")+"";
					String y1 = prop.get("PostionY")+"";
					String Length = prop.get("Length")+"";
					String Width = prop.get("Width")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					String RF = first.getFillColor().getRed()+"";
					String GF = first.getFillColor().getGreen()+"";
					String BF = first.getFillColor().getBlue()+"";
					myArray.add("Rectangle");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(Length);
					myArray.add(Width);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
					myArray.add(RF);
					myArray.add(GF);
					myArray.add(BF);
				}
				if(prop.get("Name") == 2.0) // y3ni rectangle
				{
					String x1 = prop.get("PostionX")+"";
					String y1 = prop.get("PostionY")+"";
					String Side = prop.get("Side")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					String RF = first.getFillColor().getRed()+"";
					String GF = first.getFillColor().getGreen()+"";
					String BF = first.getFillColor().getBlue()+"";
					myArray.add("Square");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(Side);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
					myArray.add(RF);
					myArray.add(GF);
					myArray.add(BF);
				}
				if(prop.get("Name") == 3.0) // y3ni rectangle
				{
					String x1 = prop.get("firstX")+"";
					String y1 = prop.get("firstY")+"";
					String x2 = prop.get("secondX")+"";
					String y2 = prop.get("secondY")+"";
					String x3 = prop.get("thirdX")+"";
					String y3 = prop.get("thirdY")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					String RF = first.getFillColor().getRed()+"";
					String GF = first.getFillColor().getGreen()+"";
					String BF = first.getFillColor().getBlue()+"";
					myArray.add("Triangle");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(x2);
					myArray.add(y2);
					myArray.add(x3);
					myArray.add(y3);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
					myArray.add(RF);
					myArray.add(GF);
					myArray.add(BF);
				}
				if(prop.get("Name") == 4.0) // y3ni rectangle
				{
					String x1 = prop.get("startX")+"";
					String y1 = prop.get("startY")+"";
					String x2 = prop.get("endX")+"";
					String y2 = prop.get("endY")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					myArray.add("Line");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(x2);
					myArray.add(y2);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
				}
				if(prop.get("Name") == 5.0) // y3ni rectangle
				{
					String x1 = prop.get("CenterX")+"";
					String y1 = prop.get("CenterY")+"";
					String Height = prop.get("Height")+"";
					String Width = prop.get("Width")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					String RF = first.getFillColor().getRed()+"";
					String GF = first.getFillColor().getGreen()+"";
					String BF = first.getFillColor().getBlue()+"";
					myArray.add("Ellipse");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(Height);
					myArray.add(Width);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
					myArray.add(RF);
					myArray.add(GF);
					myArray.add(BF);
				}
				if(prop.get("Name") == 6.0) // y3ni rectangle
				{
					String x1 = prop.get("CenterX")+"";
					String y1 = prop.get("CenterY")+"";
					String Radius = prop.get("Radius")+"";
					String RS = first.getColor().getRed()+"";
					String GS = first.getColor().getGreen()+"";
					String BS = first.getColor().getBlue()+"";
					String RF = first.getFillColor().getRed()+"";
					String GF = first.getFillColor().getGreen()+"";
					String BF = first.getFillColor().getBlue()+"";
					myArray.add("Circle");
					myArray.add(x1);
					myArray.add(y1);
					myArray.add(Radius);
					myArray.add(RS);
					myArray.add(GS);
					myArray.add(BS);
					myArray.add(RF);
					myArray.add(GF);
					myArray.add(BF);
				}
				
				myObject.put("" + counter , myArray);
				counter++;
				
			}
			try {
	            FileWriter file = new FileWriter(PathJSON);
	            file.write(myObject.toJSONString());
	            file.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}*/
	}

	@Override
	public void load(String path) {
	
		
		
		
		
		
		
		
		
		shapes.clear();
		if(path.contains(".xml"))
		{
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder dB = dBF.newDocumentBuilder();
				System.out.println(path);
				Document myDocument = dB.parse(path);
				Element main = myDocument.getDocumentElement();
				NodeList Rects = myDocument.getElementsByTagName("Shapes");
				for(int i=0;i<Rects.getLength();i++)
				{
					Node p = Rects.item(i);
					if(p.getNodeType() == Node.ELEMENT_NODE)
					{
						Element shapeIterator = (Element) p;
						if(shapeIterator.getAttribute("ID").contains("Rectangle"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Length").item(0).getTextContent();
							Double Length = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Width").item(0).getTextContent();
							Double Width = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Iterator = shapeIterator.getElementsByTagName("FillColor").item(0);
							Element iii = (Element) Iterator;
							y = iii.getElementsByTagName("FillRed").item(0).getTextContent();
							float RF = Float.parseFloat(y);
							System.out.println(RF);
							y = iii.getElementsByTagName("FillGreen").item(0).getTextContent();
							float GF = Float.parseFloat(y);
							System.out.println(GF);
							y = iii.getElementsByTagName("FillBlue").item(0).getTextContent();
							float BF = Float.parseFloat(y);
							System.out.println(BF);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Color Fill;
							try{
					        	int R = (int) RF;
					        	int G = (int) GF;
					        	int B = (int) BF;
					        	Fill = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Fill = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 1.0);
							prop.put("PostionX",x1);
							prop.put("PostionY",y1);
							prop.put("Width", Width);
							prop.put("Length", Length);
						    prop.put("ArcWidth",0.0);
						    prop.put("ArcLength", 0.0);
						    Point newe = new Point();
						    newe.x = x1.intValue();
						    newe.y = y1.intValue();
						    Shape rectangle = new RoundRectangle();
						    rectangle.setPosition(newe);
						    rectangle.setProperties(prop);
						    rectangle.setColor(Stroke);
						    rectangle.setFillColor(Fill);
						    shapes.add(rectangle);
						
						}
						if(shapeIterator.getAttribute("ID").contains("Square"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Side").item(0).getTextContent();
							Double Side = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Iterator = shapeIterator.getElementsByTagName("FillColor").item(0);
							Element iii = (Element) Iterator;
							y = iii.getElementsByTagName("FillRed").item(0).getTextContent();
							float RF = Float.parseFloat(y);
							System.out.println(RF);
							y = iii.getElementsByTagName("FillGreen").item(0).getTextContent();
							float GF = Float.parseFloat(y);
							System.out.println(GF);
							y = iii.getElementsByTagName("FillBlue").item(0).getTextContent();
							float BF = Float.parseFloat(y);
							System.out.println(BF);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Color Fill;
							try{
					        	int R = (int) RF;
					        	int G = (int) GF;
					        	int B = (int) BF;
					        	Fill = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Fill = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 2.0);
							prop.put("PostionX",x1);
							prop.put("Side", Side);
							prop.put("PostionY",y1);
						    Point newe = new Point();
						    newe.x = x1.intValue();
						    newe.y = y1.intValue();
						    Shape sqr = new Square();
						    sqr.setPosition(newe);
						    sqr.setProperties(prop);
						    sqr.setColor(Stroke);
						    sqr.setFillColor(Fill);
						    shapes.add(sqr);
						
						}
						if(shapeIterator.getAttribute("ID").contains("Circle"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Radius").item(0).getTextContent();
							Double Radius = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Iterator = shapeIterator.getElementsByTagName("FillColor").item(0);
							Element iii = (Element) Iterator;
							y = iii.getElementsByTagName("FillRed").item(0).getTextContent();
							float RF = Float.parseFloat(y);
							System.out.println(RF);
							y = iii.getElementsByTagName("FillGreen").item(0).getTextContent();
							float GF = Float.parseFloat(y);
							System.out.println(GF);
							y = iii.getElementsByTagName("FillBlue").item(0).getTextContent();
							float BF = Float.parseFloat(y);
							System.out.println(BF);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Color Fill;
							try{
					        	int R = (int) RF;
					        	int G = (int) GF;
					        	int B = (int) BF;
					        	Fill = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Fill = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 6.0);
							prop.put("CenterX",x1);
							prop.put("Radius", Radius);
							prop.put("CenterY",y1);
						    Point newe = new Point();
						    newe.x = x1.intValue();
						    newe.x = (int)(x1-Radius);
						    newe.y = (int)(y1-Radius);
						    Shape cir = new Circle();
						    cir.setPosition(newe);
						    cir.setProperties(prop);
						    cir.setColor(Stroke);
						    cir.setFillColor(Fill);
						    shapes.add(cir);
						
						}
						if(shapeIterator.getAttribute("ID").contains("Ellipse"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Height").item(0).getTextContent();
							Double Height = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("Width").item(0).getTextContent();
							Double Width = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Iterator = shapeIterator.getElementsByTagName("FillColor").item(0);
							Element iii = (Element) Iterator;
							y = iii.getElementsByTagName("FillRed").item(0).getTextContent();
							float RF = Float.parseFloat(y);
							System.out.println(RF);
							y = iii.getElementsByTagName("FillGreen").item(0).getTextContent();
							float GF = Float.parseFloat(y);
							System.out.println(GF);
							y = iii.getElementsByTagName("FillBlue").item(0).getTextContent();
							float BF = Float.parseFloat(y);
							System.out.println(BF);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Color Fill;
							try{
					        	int R = (int) RF;
					        	int G = (int) GF;
					        	int B = (int) BF;
					        	Fill = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Fill = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 5.0);
							prop.put("CenterX",x1);
							prop.put("Height", Height);
							prop.put("Width", Width);
							prop.put("CenterY",y1);
						    Point newe = new Point();
						    newe.x = (int)(x1-Width/2);
						    newe.y = (int)(y1-Height/2);
						    Shape ell = new Ellipse();
						    ell.setPosition(newe);
						    ell.setProperties(prop);
						    ell.setColor(Stroke);
						    ell.setFillColor(Fill);
						    shapes.add(ell);
						
						}
						if(shapeIterator.getAttribute("ID").contains("Triangle"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("x2").item(0).getTextContent();
							Double x2 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y2").item(0).getTextContent();
							Double y2 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("x3").item(0).getTextContent();
							Double x3 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y3").item(0).getTextContent();
							Double y3 = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Iterator = shapeIterator.getElementsByTagName("FillColor").item(0);
							Element iii = (Element) Iterator;
							y = iii.getElementsByTagName("FillRed").item(0).getTextContent();
							float RF = Float.parseFloat(y);
							System.out.println(RF);
							y = iii.getElementsByTagName("FillGreen").item(0).getTextContent();
							float GF = Float.parseFloat(y);
							System.out.println(GF);
							y = iii.getElementsByTagName("FillBlue").item(0).getTextContent();
							float BF = Float.parseFloat(y);
							System.out.println(BF);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Color Fill;
							try{
					        	int R = (int) RF;
					        	int G = (int) GF;
					        	int B = (int) BF;
					        	Fill = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Fill = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 3.0);
							prop.put("firstX",x1);
							prop.put("firstY",y1);
							prop.put("secondX",x2);
							prop.put("secondY",y2);
							prop.put("thirdX",x3);
							prop.put("thirdY",y3);
						    Point newe = new Point();
						    newe.x = x1.intValue();
						    newe.y = y1.intValue();
						    Shape tri = new Triangle();
						    tri.setPosition(newe);
						    tri.setProperties(prop);
						    tri.setColor(Stroke);
						    tri.setFillColor(Fill);
						    shapes.add(tri);
						
						}
						if(shapeIterator.getAttribute("ID").contains("Line"))
						{
							String x = shapeIterator.getElementsByTagName("x1").item(0).getTextContent();
							Double x1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y1").item(0).getTextContent();
							Double y1 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("x2").item(0).getTextContent();
							Double x2 = Double.parseDouble(x);
							x = shapeIterator.getElementsByTagName("y2").item(0).getTextContent();
							Double y2 = Double.parseDouble(x);
							Node Iterator = shapeIterator.getElementsByTagName("StrokeColor").item(0);
							Element ii = (Element) Iterator;
							String y = ii.getElementsByTagName("Red").item(0).getTextContent();
							float RS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Green").item(0).getTextContent();
							float GS = Float.parseFloat(y);
							y = ii.getElementsByTagName("Blue").item(0).getTextContent();
							float BS = Float.parseFloat(y);
							Color Stroke;
							try{
					        	int R = (int) RS;
					        	int G = (int) GS;
					        	int B = (int) BS;
					        	Stroke = new Color(R, G, B);
					        }
					        catch (Exception e){
					        	Stroke = null;
					        }
							Map prop = new HashMap<>();
							prop.put("Name", 4.0);
							prop.put("startX",x1);
							prop.put("startY",y1);
							prop.put("endX",x2);
							prop.put("endY",y2);
						    Point newe = new Point();
						    newe.x = x1.intValue();
						    newe.y = y1.intValue();
						    Shape ln = new Line();
						    ln.setPosition(newe);
						    ln.setProperties(prop);
						    ln.setColor(Stroke);
						    shapes.add(ln);
						
						}
					}
				}
				
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}catch(IOException e) {
				e.printStackTrace();
			}
			
		}
		/*else if(path.contains(".json"))
		{
			JSONParser jsonParser = new JSONParser();
	        File file = new File(path);
	        System.out.println(path);
	        try
	        {
	        	Object object = jsonParser.parse(new FileReader(file));
	            JSONObject myObject = (JSONObject) object;
	            JSONArray myArray = new JSONArray();
	            int boundaries = Integer.parseInt((String)myObject.get("Size"));
	            for(int i=0;i<boundaries;i++)
	            {
	            	myArray = (JSONArray) myObject.get(i + "");
	                String Type = new String((String) myArray.get(0));
	                Shape shape = null;
	                if(Type.contains("Rectangle"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double Length = Double.parseDouble((String) myArray.get(3));
	                	Double Width = Double.parseDouble((String) myArray.get(4));
	                	float RS = Float.parseFloat((String)myArray.get(5));
	                	float GS = Float.parseFloat((String)myArray.get(6));
	                	float BS = Float.parseFloat((String)myArray.get(7));
	                	float RF = Float.parseFloat((String)myArray.get(8));
	                	float GF = Float.parseFloat((String)myArray.get(9));
	                	float BF = Float.parseFloat((String)myArray.get(10));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Color Fill;
						try{
				        	int R = (int) RF;
				        	int G = (int) GF;
				        	int B = (int) BF;
				        	Fill = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Fill = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 1.0);
						prop.put("PostionX",x1);
						prop.put("PostionY",y1);
						prop.put("Width", Width);
						prop.put("Length", Length);
					    prop.put("ArcWidth",0.0);
					    prop.put("ArcLength", 0.0);
					    Point newe = new Point();
					    newe.x = x1.intValue();
					    newe.y = y1.intValue();
					    Shape rectangle = new RoundRectangle();
					    rectangle.setPosition(newe);
					    rectangle.setProperties(prop);
					    rectangle.setColor(Stroke);
					    rectangle.setFillColor(Fill);
					    shapes.add(rectangle);
	                }
	                if(Type.contains("Square"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double Side = Double.parseDouble((String) myArray.get(3));
	                	float RS = Float.parseFloat((String)myArray.get(4));
	                	float GS = Float.parseFloat((String)myArray.get(5));
	                	float BS = Float.parseFloat((String)myArray.get(6));
	                	float RF = Float.parseFloat((String)myArray.get(7));
	                	float GF = Float.parseFloat((String)myArray.get(8));
	                	float BF = Float.parseFloat((String)myArray.get(9));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Color Fill;
						try{
				        	int R = (int) RF;
				        	int G = (int) GF;
				        	int B = (int) BF;
				        	Fill = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Fill = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 2.0);
						prop.put("PostionX",x1);
						prop.put("Side", Side);
						prop.put("PostionY",y1);
					    Point newe = new Point();
					    newe.x = x1.intValue();
					    newe.y = y1.intValue();
					    Shape sqr = new Square();
					    sqr.setPosition(newe);
					    sqr.setProperties(prop);
					    sqr.setColor(Stroke);
					    sqr.setFillColor(Fill);
					    shapes.add(sqr);
						
	                }
	                if(Type.contains("Triangle"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double x2 = Double.parseDouble((String) myArray.get(3));
	                	Double y2 = Double.parseDouble((String) myArray.get(4));
	                	Double x3 = Double.parseDouble((String) myArray.get(5));
	                	Double y3 = Double.parseDouble((String) myArray.get(6));
	                	float RS = Float.parseFloat((String)myArray.get(7));
	                	float GS = Float.parseFloat((String)myArray.get(8));
	                	float BS = Float.parseFloat((String)myArray.get(9));
	                	float RF = Float.parseFloat((String)myArray.get(10));
	                	float GF = Float.parseFloat((String)myArray.get(11));
	                	float BF = Float.parseFloat((String)myArray.get(12));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Color Fill;
						try{
				        	int R = (int) RF;
				        	int G = (int) GF;
				        	int B = (int) BF;
				        	Fill = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Fill = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 3.0);
						prop.put("firstX", x1);
						prop.put("firstY", y1);
						prop.put("secondX", x2);
						prop.put("secondY", y2);
						prop.put("thirdX", x3);
						prop.put("thirdY", y3);
					    Point newe = new Point();
					    newe.x = x1.intValue();
					    newe.y = y1.intValue();
					    Shape tr = new Triangle();
					    tr.setPosition(newe);
					    tr.setProperties(prop);
					    tr.setColor(Stroke);
					    tr.setFillColor(Fill);
					    shapes.add(tr);
						
	                }
	                if(Type.contains("Line"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double x2 = Double.parseDouble((String) myArray.get(3));
	                	Double y2 = Double.parseDouble((String) myArray.get(4));
	                	float RS = Float.parseFloat((String)myArray.get(5));
	                	float GS = Float.parseFloat((String)myArray.get(6));
	                	float BS = Float.parseFloat((String)myArray.get(7));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 4.0);
						  prop.put("startX", x1);
						  prop.put("startY", y1);
						  prop.put("endX", x2);
						  prop.put("endY", y2);
					    Point newe = new Point();
					    newe.x = x1.intValue();
					    newe.y = y1.intValue();
					    Shape ln = new Line();
					    ln.setPosition(newe);
					    ln.setProperties(prop);
					    ln.setColor(Stroke);
					    shapes.add(ln);
					    
						
	                }
	                if(Type.contains("Ellipse"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double Length = Double.parseDouble((String) myArray.get(3));
	                	Double Width = Double.parseDouble((String) myArray.get(4));
	                	float RS = Float.parseFloat((String)myArray.get(5));
	                	float GS = Float.parseFloat((String)myArray.get(6));
	                	float BS = Float.parseFloat((String)myArray.get(7));
	                	float RF = Float.parseFloat((String)myArray.get(8));
	                	float GF = Float.parseFloat((String)myArray.get(9));
	                	float BF = Float.parseFloat((String)myArray.get(10));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Color Fill;
						try{
				        	int R = (int) RF;
				        	int G = (int) GF;
				        	int B = (int) BF;
				        	Fill = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Fill = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 5.0);
						prop.put("CenterX",x1);
						prop.put("Height", Length);
						prop.put("Width", Width);
						prop.put("CenterY",y1);
					    Point newe = new Point();
					    newe.x = (int)(x1-Width/2);
					    newe.y = (int)(y1-Length/2);
					    Shape ell = new Ellipse();
					    ell.setPosition(newe);
					    ell.setProperties(prop);
					    ell.setColor(Stroke);
					    ell.setFillColor(Fill);
					    shapes.add(ell);
						
	                }
	                if(Type.contains("Circle"))
	                {
	                	Double x1 = Double.parseDouble((String) myArray.get(1));
	                	Double y1 = Double.parseDouble((String) myArray.get(2));
	                	Double Radius = Double.parseDouble((String) myArray.get(3));
	                	float RS = Float.parseFloat((String)myArray.get(4));
	                	float GS = Float.parseFloat((String)myArray.get(5));
	                	float BS = Float.parseFloat((String)myArray.get(6));
	                	float RF = Float.parseFloat((String)myArray.get(7));
	                	float GF = Float.parseFloat((String)myArray.get(8));
	                	float BF = Float.parseFloat((String)myArray.get(9));
	                	Color Stroke;
						try{
				        	int R = (int) RS;
				        	int G = (int) GS;
				        	int B = (int) BS;
				        	Stroke = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Stroke = null;
				        }
						Color Fill;
						try{
				        	int R = (int) RF;
				        	int G = (int) GF;
				        	int B = (int) BF;
				        	Fill = new Color(R, G, B);
				        }
				        catch (Exception e){
				        	Fill = null;
				        }
						Map prop = new HashMap<>();
						prop.put("Name", 6.0);
						prop.put("CenterX",x1);
						prop.put("Radius", Radius);
						prop.put("CenterY",y1);
					    Point newe = new Point();
					    newe.x = x1.intValue();
					    newe.x = (int)(x1-Radius);
					    newe.y = (int)(y1-Radius);
					    Shape cir = new Circle();
					    cir.setPosition(newe);
					    cir.setProperties(prop);
					    cir.setColor(Stroke);
					    cir.setFillColor(Fill);
					    shapes.add(cir);
						
	                }
	                
	            }
	            
	            
	        } catch (Exception e) {
	            throw new RuntimeException();
	        }
		}*/
		previous.clear();
		ptr=0;
		LinkedList<Shape> newshapes =new<Shape> LinkedList();
		for(Shape x:shapes){
			newshapes.add(x);
		}
		memento state=new memento(newshapes);
		
		previous.add(ptr,state);
		
		while(previous.size()!=(ptr+1)){
			previous.removeLast();
			
		}
		
		while(previous.size()>21){
			previous.removeFirst();
			ptr=previous.size()-1;//19
		}
		
	}
	
}

