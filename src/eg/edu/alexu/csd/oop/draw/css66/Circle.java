package eg.edu.alexu.csd.oop.draw.css66;


import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Circle implements Shape {

    protected Point p;
    protected Map<String, Double> prop;
    protected Color c=Color.black;
    protected Color fc=Color.RED;
    Graphics g;
    public Circle() {
        prop = new HashMap<>();
        prop.put("Radius", 0.0);
        prop.put("Name", 6.0);
        p=new Point();
        p.x=0;
        p.y=0;
    }
			
    @Override
    public void setPosition(Point position) {
        p = position;
    }

    @Override
    public Point getPosition() {
        return p;
    }

    @Override
    public void setProperties(Map<String, Double> properties) {
        prop = properties;
    }

    @Override
    public Map<String, Double> getProperties() {
        return prop;
    }

    @Override
    public void setColor(Color color) {
        c = color;
    }

    @Override
    public Color getColor() {
        return c;
    }

    @Override
    public void setFillColor(Color color) {
        fc = color;
    }

    @Override
    public Color getFillColor() {
        return fc;
    }

    @Override
    public void draw(Graphics canvas) {
    	
        ((Graphics2D) canvas).setColor(getFillColor());
        ((Graphics2D) canvas).fillOval((int) p.getX(),(int) p.getY(), (int)2* prop.get("Radius").intValue(),(int)2* prop.get("Radius").intValue());
        

        ((Graphics2D) canvas).setStroke(new BasicStroke(2));
        ((Graphics2D) canvas).setColor(getColor());
        ((Graphics2D) canvas).drawOval((int) p.getX(),(int) p.getY(),(int)2* prop.get("Radius").intValue(),(int)2* prop.get("Radius").intValue());
       
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Shape r = new Circle();
        r.setColor(c);
        r.setFillColor(fc);
        r.setPosition(p);
        Map newprop = new HashMap<>();
        for (Map.Entry s: prop.entrySet())
            newprop.put(s.getKey(), s.getValue());
        r.setProperties(newprop);
        return r;
    }
   
}

