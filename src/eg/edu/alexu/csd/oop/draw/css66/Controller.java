package eg.edu.alexu.csd.oop.draw.css66;



import javax.swing.*;


import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;
import eg.edu.alexu.csd.oop.draw.Shape;
public class Controller {
	
	Point start,end;
	View view;
	Model model;
	int num = 0;
	double arr[][] = new double[3][2];
	
	public Controller(View aa,Model ab){
		view=aa;
		model=ab;
		view.addMouseAdabter(new MouseEvents1(),new MouseEvent2());
		view.addAL(new copyListener(),new selectShapeListener(),new plugListener(),new lodListener(),new savListener(),new redoListener(),new undoListener(),new removeListener(),new updateListener(),new rectListener(),new sqrListener(),new cirListener(),new lineListener(),new ellipseListener(),new triListener(),new strokeListener(),new fillListener(),new comboBoxListener());
		
	}
	class copyListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(model.shapes.size()!=0){
				try {
					
					Shape newe = (Shape) model.shapes.get(model.selected).clone();
					model.shapes.add(newe);
					model.noShapes++;
					view.Updatecombobox(model.shapes.size());
					view.display(newe.getProperties());
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}
	class selectShapeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			model.Indicator=7+view.externalShapes.getSelectedIndex();
			
		}
		
	}
	class plugListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			JFileChooser fs = new JFileChooser();
			File file;
			String path=null;
			int stat =fs.showOpenDialog(null);
			if(stat==fs.APPROVE_OPTION){
				file=fs.getSelectedFile();
				path=file.getAbsolutePath();
			}
			System.out.println("before call");
			try{
			model.plugIn(path);
			}
			catch (Exception e){
				System.out.println("some error happened");
			}
			System.out.println("after call");
			view.updatesupportedShapes();
			System.out.println("after call view");
		}
		
	}
	class savListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				JFileChooser chooser = new JFileChooser();
				String path = null;
				int status = chooser.showSaveDialog(null);
				 chooser.setDialogTitle("Choose a Place to Save File");
				if (status == JFileChooser.APPROVE_OPTION) {
		            File file = chooser.getSelectedFile();
		            path = file.getAbsolutePath();
		        } else {
		            path = null;
		        }
				model.save(path);
			}
			catch(Exception e2){
				
			}
		}
		
	}
	
	class lodListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				JFileChooser chooser = new JFileChooser();
				String path;
				int status =chooser.showOpenDialog(null);
				if(status == JFileChooser.APPROVE_OPTION)
				{
					File file = chooser.getSelectedFile();
					path = file.getAbsolutePath();
				}
				else
				{
					path = null;
				}
				
				model.load(path);
				if(model.shapes.size()!=0)
				{
					int i=1;
					ListIterator<Shape> Iterator = model.shapes.listIterator();
					Shape current = model.shapes.getFirst();
					while(Iterator.hasNext())
					{
						current = Iterator.next();
						view.rebuildScreen();
						view.display(current.getProperties());
						i++;
					}
				}
//				view.Updatecombobox(model.shapes.size());
				view.display(model.shapes.get(model.selected).getProperties());
				view.repaint();
			}catch(Exception e2){
				
			}
			
		}
		
	}
	class undoListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try{
			model.undo();
			
			view.rebuildScreen();
			view.repaint();
			}
			catch(Exception e2){
				// set enable false to be done
			}
			
		}
		
	}
	class redoListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try{
			model.redo();
			
			view.rebuildScreen();
			view.repaint();
			}
			catch(Exception e2){
				// set enable false.. to be done
			}
			
		}
		
	}
	class removeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			// TODO Auto-generated method stub
			if(model.shapes.size()>0){
			
			model.removeShape(model.shapes.get(model.selected));
			
			view.repaint();
			view.removeselected();
			
			}
		}
		
	}
	class updateListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
		
			if(model.shapes.size()>0){
			Shape old=model.shapes.get(model.selected);
			Shape newe;
			if(old instanceof RoundRectangle){
					newe=new RoundRectangle();
					newe.setProperties(view.getdata());
					newe.setColor(model.strokeColor);
					newe.setFillColor(model.fillColor);
					Point p=new Point();
					double x=newe.getProperties().get("PostionX");
					double y=newe.getProperties().get("PostionY");
					p.y=(int)y;
					p.x=(int)x;
					newe.setPosition(p);
					model.updateShape(old, newe);
					view.repaint();
					
			}else if(old instanceof Square){
				newe=new Square();
				newe.setProperties(view.getdata());
				newe.setColor(model.strokeColor);
				newe.setFillColor(model.fillColor);
				Point p=new Point();
				double x=newe.getProperties().get("PostionX");
				double y=newe.getProperties().get("PostionY");
				p.y=(int)y;
				p.x=(int)x;
				newe.setPosition(p);
				model.updateShape(old, newe);
				view.repaint();
			}else if(old instanceof Triangle){
				newe=new Triangle();
				newe.setProperties(view.getdata());
				newe.setColor(model.strokeColor);
				newe.setFillColor(model.fillColor);
				double x1=newe.getProperties().get("firstX");
				double y1=newe.getProperties().get("firstY");
				Point p=new Point();
				p.x=(int)x1;
				p.y=(int)y1; 
				newe.setPosition(p);
				model.updateShape(old, newe);
				view.repaint();
				
			}else if(old instanceof Circle){
				newe=new Circle();
				newe.setProperties(view.getdata());
				newe.setColor(model.strokeColor);
				newe.setFillColor(model.fillColor);
				Point p=new Point();
				double x=newe.getProperties().get("CenterX")-newe.getProperties().get("Radius");
				double y=newe.getProperties().get("CenterY")-newe.getProperties().get("Radius");
				p.y=(int)y;
				p.x=(int)x;
				newe.setPosition(p);
				model.updateShape(old, newe);
				view.repaint();
			}else if(old instanceof Ellipse){
				newe=new Ellipse();
				newe.setProperties(view.getdata());
				newe.setColor(model.strokeColor);
				newe.setFillColor(model.fillColor);
				Point p=new Point();
				double x=newe.getProperties().get("CenterX")-newe.getProperties().get("Width")/2.0;
				double y=newe.getProperties().get("CenterY")-newe.getProperties().get("Height")/2.0;
				p.y=(int)y;
				p.x=(int)x;
				newe.setPosition(p);
				model.updateShape(old, newe);
				view.repaint();
			}else if(old instanceof Line){
				newe=new Line();
				newe.setProperties(view.getdata());
				newe.setColor(model.strokeColor);
				newe.setFillColor(model.fillColor);
				double x1=newe.getProperties().get("startX");
				double y1=newe.getProperties().get("startY");
				Point p=new Point();
				p.x=(int)x1;
				p.y=(int)y1; 
				newe.setPosition(p);
				model.updateShape(old, newe);
				view.repaint();
			}else{
				int size =model.supportedShapes.size();
				System.out.println("plugIn success");
				for(int i=0;i<size;i++){
					Class x = model.supportedShapes.get(i);
					if(x.isInstance(old)){
						 try {
							newe = (Shape) x.newInstance();
							newe.setProperties(view.getdata());
							newe.setColor(model.strokeColor);
							newe.setFillColor(model.fillColor);
							model.updateShape(old, newe);
							view.repaint();
						} catch (InstantiationException | IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			
			
		}
		
	}}
	class comboBoxListener implements ItemListener{

		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub
				
				model.selected=view.itemList.getSelectedIndex();
				 
				if(model.selected!=-1&&model.selected<model.shapes.size()){
					view.display(model.shapes.get(model.selected).getProperties());
					
				}
		}
		
	}
	
	class rectListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 1;
			
		}
		
	}
	
	
	
	class sqrListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 2;
			
		}
		
	}
	
	class cirListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 3;
			
		}
		
	}
	
	class lineListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 4;
			
		}
		
	}
	
	class ellipseListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 5;
			
		}
		
	}
	
	class triListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.Indicator = 6;
			
		}
		
	}
	
	class strokeListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
				 model.strokeColor = JColorChooser.showDialog(null,  "Pick a Stroke", Color.BLACK);
			
		}
		
	}
	
	class fillListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			model.fillColor = JColorChooser.showDialog(null,  "Pick a Fill", Color.BLACK);
			
		}
		
	}
	
	
	
	
	class MouseEvents1 implements MouseListener
	{
		  public void mousePressed(MouseEvent e)
          {
          	
          	// When the mouse is pressed get x & y position
			  start=new Point(e.getX(),e.getY());
			  end=start;
            }
		  
		  public void mouseReleased(MouseEvent e)
          {
			 
				  	model.noShapes++;
			  
			  
			  if(model.Indicator == 1)
			  {
				//get properties of rectangle 
				  Map prop = model.rectangleprop(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape rectangle=new RoundRectangle();
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  newe.y=(int) Math.min(start.getY(), e.getY());
				  rectangle.setPosition(newe);
				  rectangle.setProperties(prop);
				  rectangle.setColor(model.strokeColor);
				  rectangle.setFillColor(model.fillColor);
				  //adding shape
				  model.addShape(rectangle);
				  //draw
				  
				 view.Updatecombobox(model.shapes.size());
				  view.board.repaint();
				  view.display(rectangle.getProperties());
				  
				  System.out.println(e.getX()+" "+e.getY());
			  }
			  else if(model.Indicator == 2)
			  {
				//get properties of rectangle 
				  Map prop = model.squareProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape square=new Square();
				  //get top left point which is going to be set
				  Point newe=new Point();
				  
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  
				  if(e.getY()<start.getY()){
					  newe.y=(int)(start.getY()-Math.abs(start.getX()-e.getX()));
				  }else{
					  newe.y=(int)(start.getY());
				  }
				  
				  square.setPosition(newe);
				  square.setProperties(prop);
				  square.setColor(model.strokeColor);
				  square.setFillColor(model.fillColor);
				  //adding shape
				  model.addShape(square);
				  //draw
				  view.board.repaint();
				  view.Updatecombobox(model.shapes.size());
				  view.display(square.getProperties());
				  System.out.println(e.getX()+" "+e.getY());
			  }
			  else if(model.Indicator == 3)
			  {
				//get properties of rectangle 
				  Map prop = model.circleProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape circle=new Circle();
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) ( start.getX()-Math.hypot(start.getX()-e.getX(), start.getY()-e.getY()));
				  newe.y=(int) ( start.getY()-Math.hypot(start.getX()-e.getX(), start.getY()-e.getY()));
				  circle.setPosition(newe);
				  circle.setProperties(prop);
				  circle.setColor(model.strokeColor);
				  circle.setFillColor(model.fillColor);
				  //adding shape
				  model.addShape(circle);
				  //draw
				  view.Updatecombobox(model.shapes.size());
				  view.board.repaint();
				  view.display(circle.getProperties());
				  System.out.println(e.getX()+" "+e.getY());
			  }
			  else if(model.Indicator == 4)
			  {
				//get properties of rectangle 
				  Map prop = model.lineProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape line=new Line();
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  newe.y=(int) Math.min(start.getY(), e.getY());
				  line.setPosition(newe);
				  line.setProperties(prop);
				  line.setColor(model.strokeColor);
				  //adding shape
				  model.addShape(line);
				  //draw
				  view.Updatecombobox(model.shapes.size());
				  view.board.repaint();
				  view.display(line.getProperties());
				  System.out.println(e.getX()+" "+e.getY());
			  }
			  else if(model.Indicator == 5)
			  {
				//get properties of rectangle 
				  Map prop = model.ellipseProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape ellipse=new Ellipse();
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) ((int)start.getX()- Math.abs(start.getX()-e.getX()));
				  newe.y=(int) ((int)start.getY()- Math.abs(start.getY()- e.getY()));
				  ellipse.setPosition(newe);
				  ellipse.setProperties(prop);
				  ellipse.setColor(model.strokeColor);
				  ellipse.setFillColor(model.fillColor);
				  //adding shape
				  model.addShape(ellipse);
				  //draw
				  view.Updatecombobox(model.shapes.size());
				  view.board.repaint();
				  view.display(ellipse.getProperties());
				  System.out.println(e.getX()+" "+e.getY());
			  }
			  else if(model.Indicator == 6)
				{
					arr[num][0] = e.getX();
					arr[num][1] = e.getY();
					System.out.println(num);
					  if(num == 2) {
						  Shape triangle=new Triangle();
						  Map prop = model.triProp(arr[0][0],arr[0][1],arr[1][0],arr[1][1],arr[2][0],arr[2][1]);
						  Point st = new Point();
						  st.x = (int)arr[0][0];
						  st.y = (int)arr[0][1];
						  triangle.setPosition(st);
						  triangle.setProperties(prop);
						  triangle.setColor(model.strokeColor);
						  triangle.setFillColor(model.fillColor);
						  //adding shape
						  model.addShape(triangle);
						  //draw
						  view.Updatecombobox(model.shapes.size());
						  view.board.repaint();
						  view.display(triangle.getProperties());
						  num = 0;
					  }
					  else {
						  num++;
					  }
					
				}else if(model.Indicator!=-1){//not at start state so its external shape
						int index= model.Indicator-7;
						try {
							Shape newe=model.getSupportedShapes().get(index).newInstance();
							model.addShape(newe);
							view.Updatecombobox(model.shapes.size());
							view.board.repaint();
							 view.display(newe.getProperties());
						} catch (InstantiationException | IllegalAccessException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
			 
          }

		@Override
		public void mouseClicked(MouseEvent e) {
			
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
	
	class MouseEvent2 implements MouseMotionListener{

		@Override
		public void mouseDragged(MouseEvent e) 
		{
			
			  if(model.Indicator == 1)
			  {
				//get properties of rectangle 
				  Map prop = model.rectangleprop(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape rectangle=new RoundRectangle();
				  rectangle.setFillColor(java.awt.Color.darkGray);
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  newe.y=(int) Math.min(start.getY(), e.getY());
				  rectangle.setPosition(newe);
				  rectangle.setProperties(prop);
				  //adding shape
				  model.setguide(rectangle);
				  //draw
				  view.board.repaint();
				  view.display(rectangle.getProperties());
			  }
			  else if(model.Indicator == 2)
			  {
				//get properties of rectangle 
				  Map prop = model.squareProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape square=new Square();
				  square.setFillColor(java.awt.Color.darkGray);
				  //get top left point which is going to be set
				  Point newe=new Point();
				  
				  
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  
				  if(e.getY()<start.getY()){
					  newe.y=(int)(start.getY()-Math.abs(start.getX()-e.getX()));
				  }else{
					  newe.y=(int)(start.getY());
				  }
				  
				  
				  square.setPosition(newe);
				  square.setProperties(prop);
				  //adding shape
				  model.setguide(square);
				  //draw
				  view.board.repaint();
				  view.display(square.getProperties());
			  }
			  else if(model.Indicator == 3)
			  {
				//get properties of rectangle 
				  Map prop = model.circleProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape circle=new Circle();
				  circle.setFillColor(java.awt.Color.darkGray);
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) ( start.getX()-Math.hypot(start.getX()-e.getX(), start.getY()-e.getY()));
				  newe.y=(int) ( start.getY()-Math.hypot(start.getX()-e.getX(), start.getY()-e.getY()));
				  circle.setPosition(newe);
				  circle.setProperties(prop);
				  //adding shape
				  model.setguide(circle);
				  //draw
				  view.board.repaint();
				  view.display(circle.getProperties());
			  }
			  
			  else if(model.Indicator == 4)
			  {
				//get properties of rectangle 
				  Map prop = model.lineProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape line=new Line();
				  line.setColor(java.awt.Color.darkGray);
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) Math.min(start.getX(), e.getX());
				  newe.y=(int) Math.min(start.getY(), e.getY());
				  line.setPosition(newe);
				  line.setProperties(prop);
				  //adding shape
				  model.setguide(line);
				  //draw
				  view.board.repaint();
				  view.display(line.getProperties());
			  }
			  
			  else if(model.Indicator == 5)
			  {
				//get properties of rectangle 
				  Map prop = model.ellipseProp(start.getX(),start.getY(),e.getX(),e.getY()) ;
				  Shape ellipse=new Ellipse();
				  ellipse.setFillColor(java.awt.Color.darkGray);
				  //get top left point which is going to be set
				  Point newe=new Point();
				  newe.x=(int) ((int)start.getX()- Math.abs(start.getX()-e.getX()));
				  newe.y=(int) ((int)start.getY()- Math.abs(start.getY()- e.getY()));
				  ellipse.setPosition(newe);
				  ellipse.setProperties(prop);
				  //adding shape
				  model.setguide(ellipse);
				  //draw
				  view.board.repaint();
				  view.display(ellipse.getProperties());
			  }
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if(model.Indicator == 6)
			{
				if(num == 1)
				{
					Map prop = model.lineProp(arr[0][0],arr[0][1],e.getX(),e.getY()) ;
					  Shape line=new Line();
					  line.setColor(java.awt.Color.darkGray);
					  //get top left point which is going to be set
					  Point newe=new Point();
					  newe.x=(int) Math.min(start.getX(), e.getX());
					  newe.y=(int) Math.min(start.getY(), e.getY());
					  line.setPosition(newe);
					  line.setProperties(prop);
					  //adding shape
					  model.setguide(line);
					  //draw
					  view.board.repaint();
					  view.display(line.getProperties());
				}
				if(num == 2)
				{
					
					  
					  Shape triangle=new Triangle(); 
					  Map prop = model.triProp(arr[0][0],arr[0][1],arr[1][0],arr[1][1],e.getX(),e.getY());
					  Point st = new Point();
					  st.x = (int)arr[0][0];
					  st.y = (int)arr[0][1];
					  triangle.setPosition(st);
					  triangle.setProperties(prop);
					  triangle.setColor(model.strokeColor);
					  triangle.setFillColor(model.fillColor);
					  //adding shape
					  model.setguide(triangle);
					  //draw
					  view.board.repaint();
					  view.display(triangle.getProperties());
				}
			}
		}
		
	}
	 
}


