package eg.edu.alexu.csd.filestructure.sort.cs66;

import java.util.ArrayList;
import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyHeap<T extends Comparable<T>> implements IHeap<T>{
	ArrayList <INode<T>>contain ;
	
	private int size =0;
	@Override
	public INode getRoot() {
		
		return contain.get(0);
	}

	@Override
	public int size() {
		
		return this.size;
	}

	@Override
	public void heapify(INode node) {
		
		int index = ((MyNode<T>)node).index;
		int r=2*index+1;
		int l =2*index+2;
		int  largest=index;
	
		if(r<size&&contain.get(r).getValue().compareTo(contain.get(largest).getValue())>0) {
			largest =r;
		}
		if(l<size&&contain.get(l).getValue().compareTo(contain.get(largest).getValue())>0) {
			largest =l;
		}
		if(largest != index) {
			
			T temp = contain.get(largest).getValue();
			contain.get(largest).setValue(contain.get(index).getValue());
			contain.get(index).setValue(temp);
			heapify(contain.get(largest));
		}
		
		
	}

	@Override
	public T extract() {
		if(size==0) {
			throw null;
		}
		T out = contain.get(0).getValue();
		contain.get(0).setValue(contain.get(size-1).getValue());
		contain.remove(size-1);
		size--;
		if(size!=0)heapify(contain.get(0));
		
		return out;
	}

	@Override
	public void insert(Comparable element) {
		T element2 =(T)element;
		MyNode <T> curr = new MyNode<T>();
		curr.setValue(element2);
		contain.add(curr);
		size++;
		if(size>1)curr.parent= contain.get((size-1)/2);
		while(curr.parent!= null &&curr.getParent().getValue().compareTo(curr.getValue())<0) {
			T temp =curr.getValue();
			curr.setValue(curr.parent.getValue());
			curr.parent.setValue(temp);
			curr=(MyNode<T>) curr.parent;
		}
		
	}

	@Override
	public void build(Collection unordered) {
		ArrayList <T> container= new ArrayList<T>(unordered);
		contain = new ArrayList<INode<T>>();
		for(int i=0;i<container.size();i++) {
			MyNode curr = new MyNode();
			curr.setValue(container.get(i));
			curr.index=i;
			contain.add(curr);
		}
		for(int i=0;i<contain.size();i++) {
			MyNode curr = (MyNode)contain.get(i);
			if(i>0) curr.parent=contain.get((i-1)/2);
			if(i*2+1<contain.size()) curr.left=contain.get(i*2+1);
			if(i*2+2<contain.size()) curr.right=contain.get(i*2+2);
			
		}
		size =contain.size();
		for(int i =size/2-1;i>=0;i--) {
			heapify(contain.get(i));
		}
		
		
		
	}

}
