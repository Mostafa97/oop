package eg.edu.alexu.csd.filestructure.sort.cs66;

import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyNode <T extends Comparable<T>> implements INode<T> {
	INode	<T> left =null;
	INode <T> right = null;
	INode <T> parent= null;
	int index;
	T val= null;
	
	@Override
	public INode<T> getLeftChild() {
		
		return this.left;
	}

	@Override
	public INode<T> getRightChild() {
	
		return this.right;
	}

	@Override
	public INode <T> getParent() {
		
		return parent;
	}

	@Override
	public T getValue() {
		
		return val;
	}

	@Override
	public void setValue(T value) {
		val = (T)value;
		
	}
	
}
